#!/usr/bin/env ruby

# * CREATE A CHARTER BUSINESS ZIP FILE 
# *  SUITABLE FOR UPLOAD TO ATG BCC

# must be run from the git root directory

require 'time'

# set project variables
#  - see doc/dlp-howto.txt
PROJECT = 	'spectrum-lp'

if !File.directory? 'build' 
	abort '** run from the git root directory'
end


`rm -rf zip/build && rm -rf zip/charter.business.com`
if $?.success?
	puts "* temp directory cleared"
end

`rsync -Rrv build/mediacontent/spectrum-campaign-q1-2015/ zip/`
if $?.success?
	puts "* files copied to temp directory"
end

`mv zip/build/ zip/charter.business.com/`
if $?.success?
	puts "* directory renamed"
end

`find zip/ -name ".DS*" -type f|xargs rm -f`
if $?.success?
	puts "* .DS_Store files removed"
end

zipfile = "#{PROJECT}-#{Time.now.localtime.iso8601.gsub(':','.')}.zip"

`cd zip && zip -r #{zipfile} charter.business.com/ && cd ..`
if $?.success?
	puts "* zip file #{zipfile} created!!"
end
