


$(document).ready(function (){
	
	$("#phoneNumber").keyup(function(){     
		formatPhone(this);    
	  });
	
	$("#zipCode").keyup(function(){     
		formatZipCode(this);	    
	  });
	
	$("button#submitFormGreenButton").on("click",function(){
		$("input#submitFormButton").click();
	});
	
	$("#Phone__c").keyup(function(){     
		formatPhone(this);    
	  });
	
});

/**
 * Success method.
 */
function leadGenFormSuccess(data) {
	var response = data.component.restResponse;
	
	// normal path
    if (response.success) {
    	
    	resetLeadGenFormFields() ;
    	
    	if (response.submittedBefore) {
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').text(
					"The form is already submited");
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').attr('style',
					'color: red');
		} else {
			window.location = '/content/thank-you-contact-us';
		}
    }

	// evaluate error
    else {
    	 
    	 var errorMessageHtml = "";
    	    for (var i=0;i<data.component.restResponse.errors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.errors[i].message + " " + data.component.restResponse.errors[i].errorCode + "<br/>";
    	    }
    	    for (var i=0;i<data.component.restResponse.fieldErrors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.fieldErrors[i].message + " " + data.component.restResponse.fieldErrors[i].errorCode + "<br/>";
    	    }
    	   
    	    resetLeadGenFormFields() ;
                
    	    if(errorMessageHtml.indexOf("businessName", 0)>0){
	            $('#businessName').attr('class','error-box');
	            $('#businessName-error-txt').html("Please enter the contact’s business name.");
	        }
	    
    	    if(errorMessageHtml.indexOf("firstName", 0)>0){
    	            $('#firstName').attr('class','error-box');
    	            $('#firstName-error-txt').html("Please enter the contact's first name.");
    	    }
    	    if(errorMessageHtml.indexOf("lastName", 0)>0){
	            $('#lastName').attr('class','error-box');
	            $('#lastName-error-txt').html("Please enter the contact's last name.");
	        }
	    
    	    if(errorMessageHtml.indexOf("zipCode", 0)>0){
	            $('#zipCode').attr('class','error-box');
	            $('#zipCode-error-txt').html("Please enter a valid zip code.");
	        }
	        if(errorMessageHtml.indexOf("emailAddress", 0)>0){
            $('#emailAddress').attr('class','error-box');
            $('#emailAddress-error-txt').html("Please enter a valid email address.");
            }
	        if(errorMessageHtml.indexOf("phone", 0)>0){
	            $('#phoneNumber').attr('class','error-box');
	            $('#phoneNumber-error-txt').html("Please enter a valid phone number.");
	        }
    }
}


function resetLeadGenFormFields() {
	  $('#businessName').attr('class','businessName');
      $('#businessName-error-txt').html("");  	    
      
      $('#firstName').attr('class','firstName');
      $('#firstName-error-txt').html("");

      $('#lastName').attr('class','lastName');
      $('#lastName-error-txt').html("");

      $('#zipCode').attr('class','zipCode');
      $('#zipCode-error-txt').html("");

      $('#emailAddress').attr('class','emailAddress');
      $('#emailAddress-error-txt').html("");
      
      $('#phoneNumber').attr('class','phoneNumber');
      $('#phoneNumber-error-txt').html("");
      
     
      $('#contactInfosResultDiv').html("");
      $('#hereWeDisplayIfTheFormIsAlreadySubmitted').html("");
     
}