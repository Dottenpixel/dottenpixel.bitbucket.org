$(document).ready(function (){
	$("button#submitFormGreenButton").on("click",function(){
		$("input#submitFormButton").click();
	});
	
});

/**
 * Success method.
 */
function submitASupportCaseFormSuccess(data) {
	var response = data.component.restResponse;
	
	// normal path
    if (response.success) {
    	
    	resetContactUsFormFields() ;
    	
    	if (response.submittedBefore) {
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').text(
					"The form is already submited");
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').attr('style',
					'color: red');
		} else {
			window.location = '/content/thank-you-contact-us';
		}
    }

	// evaluate error 
    else {
    	 
    	 var errorMessageHtml = "";
    	    for (var i=0;i<data.component.restResponse.errors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.errors[i].message + " " + data.component.restResponse.errors[i].errorCode + "<br/>";
    	    }
    	    for (var i=0;i<data.component.restResponse.fieldErrors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.fieldErrors[i].message + " " + data.component.restResponse.fieldErrors[i].errorCode + "<br/>";
    	    }
    	   
    	    resetContactUsFormFields() ;
    	    
    	    if(errorMessageHtml.indexOf("reasonForContact", 0)>0){
	            $('#reasonForContact').attr('class','error-box');
	            $('#reasonForContact-error-txt').html("Please enter a reason for contact.");
	        }
    	    
    	    if(errorMessageHtml.indexOf("businessName", 0)>0){
	            $('#businessName').attr('class','error-box');
	            $('#businessName-error-txt').html("Please enter the contact’s business name.");
	        }
	    
    	    if(errorMessageHtml.indexOf("firstName", 0)>0){
    	            $('#firstName').attr('class','error-box');
    	            $('#firstName-error-txt').html("Please enter the contact's first name.");
    	    }
    	    if(errorMessageHtml.indexOf("lastName", 0)>0){
	            $('#lastName').attr('class','error-box');
	            $('#lastName-error-txt').html("Please enter the contact's last name.");
	        }
    	    if(errorMessageHtml.indexOf("phonenumber", 0)>0){
	            $('#phoneNumber').attr('class','error-box');
	            $('#phone-error-txt').html("Please enter the contact's phone number.");
	        }
	    
	        if(errorMessageHtml.indexOf("emailAddress", 0)>0){
	        	$('#emailAddress').attr('class','error-box');
	        	$('#emailAddress-error-txt').html("Please enter a valid email address.");
            }
	        
	        if(errorMessageHtml.indexOf("confirmEmail", 0)>0){
	        	$('#confirmEmailAddress').attr('class','error-box');
	        	$('#confirmEmailAddress-error-txt').html("Please confirm your email address.");
            }
	        
	        if(errorMessageHtml.indexOf("address1", 0)>0){
	        	$('#address1').attr('class','error-box');
	        	$('#address1-error-txt').html("Please enter a valid address.");
            }
	        
	        if(errorMessageHtml.indexOf("city", 0)>0){
	        	$('#city').attr('class','error-box');
	        	$('#city-error-txt').html("Please enter a valid city.");
            }
	        
	        if(errorMessageHtml.indexOf("state", 0)>0){
	        	$('#state').attr('class','error-box');
	        	$('#state-error-txt').html("Please enter a valid state.");
            }
	        
	        if(errorMessageHtml.indexOf("zip", 0)>0){
	        	$('#zip').attr('class','error-box');
	        	$('#zip-error-txt').html("Please enter a valid zip code.");
	        }
    }
}


function resetContactUsFormFields() {
	  $('#reasonForContact').attr('class','');
	  $('#reasonForContact-error-txt').html("");
	 
	  $('#businessName').attr('class','businessName');
	  $('#businessName-error-txt').html("");  	    
	  
	  $('#firstName').attr('class','firstName');
	  $('#firstName-error-txt').html("");
	            
	  $('#lastName').attr('class','lastName');
	  $('#lastName-error-txt').html("");
	      
	  $('#phoneNumber').attr('class','');
	  $('#phone-error-txt').html("");
	      
	  $('#emailAddress').attr('class','emailAddress');
	  $('#emailAddress-error-txt').html("");
	  
	  $('#confirmEmail').attr('class','');
	  $('#confirmEmailAddress-error-txt').html("");
	  
	  $('#address1').attr('class','');
	  $('#address1-error-txt').html("");
	  
	  $('#city').attr('class','');
	  $('#city-error-txt').html("");
	  
	  $('#state').attr('class','state');
	  $('#state-error-txt').html("");
	  
	  $('#zip').attr('class','');
	  $('#zip-error-txt').html("");
	  
	  $('#submitInfosResultDiv').html("");
      $('#hereWeDisplayIfTheFormIsAlreadySubmitted').html("");
     
}