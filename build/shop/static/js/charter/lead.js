$(document).ready(function(){
   
    if ($('#buy_flow_lead_gen_form').length) {
        // populate the lead page and fill input with information in session
      if($('#Company').val() == ""){
            $('#Company').val(USEREXPERIENCE.getCookie('Company'));
        }
        if ($('#FullName').val() == "") {
            $('#FullName').val(USEREXPERIENCE.getCookie('FullName'));
        }
        if ( $('#Email').val() == "") {
            $('#Email').val(USEREXPERIENCE.getCookie('Email'));
        }
        if ($('#Phone').val() == "" ) {
            $('#Phone').val(formatPhoneNumber(USEREXPERIENCE.getCookie('Phone')));
        }
    }

});


var LEAD = typeof LEAD === 'undefined' ? {} : LEAD;

LEAD.invalidZip = "INVALID_ZIP";

LEAD.processForm = function(formId, successFunction, errorFunction,ajaxErrorFunction, headerFieldsFormId, displayValidationError) {
	FORM.clearErrors('errorDiv');
	var requiredFieldsInError = FORM.checkRequiredFields(formId);
	if (requiredFieldsInError.length > 0) {
		// if displayValidationError is undefined or true we need to display error (this option it used because errors validation are already processed)
		if (displayValidationError == undefined || displayValidationError == true) {
			FORM.showRequiredErrors(requiredFieldsInError, LEAD.validationMessages());
		}
	} else {
		LEAD.insert(formId, successFunction, errorFunction,ajaxErrorFunction, headerFieldsFormId, displayValidationError);
	}
};

LEAD.insert = function(formId, successFunction, errorFunction, ajaxErrorFunction, headerFieldsFormId, displayValidationError) {
	// no error function then use the default error function
	
	if (!$.isFunction(ajaxErrorFunction)) {
		ajaxErrorFunction = ajaxErrorDefault;
	}

	if (headerFieldsFormId == undefined) {
		headerFieldsFormId = 'insertLeadHeaderFieldsForm';
	}

	//populate previous URL
	if(($('[name="LeadReferredByUrl__c"]')).length > 0 ){
	   $('[name="LeadReferredByUrl__c"]').each( function() {
	      // overwrite lead submitted by url only if it's not already populated
	      if ($(this).val().length <= 0){
	         $(this).val(document.referrer); 
	      }
	   }); 
	}
	// get referred by from cookies already defined in landing-pages
	var leadRef = USEREXPERIENCE.getCookie("leadRef");
	if (leadRef != "") {
		// if not empty reset LeadReferredByUrl__c
		if(($('[name="LeadReferredByUrl__c"]')).length > 0 ){
			$('[name="LeadReferredByUrl__c"]').each( function() { 
				 $(this).val(leadRef); 
				}); 
		}
	}
	
     //populate siteweb (this field help us to identify the form used to create the Lead)
    if(($('[name="Website"]')).length > 0 ){
         $('[name="Website"]').each( function() {
           if ($(this).val().length <= 0){
                  var website = window.location.href;
                  $(this).val(website.slice(0,250)); 
           }
         }); 
       }else{
        var website = window.location.href;
        $('#'+formId).append('<input id="Website" name="Website" value="'+website.slice(0,250)+'"  type="hidden">');
       } 
	
	var headerFieldsSerialized = $('#'+headerFieldsFormId+' :input').serializeArray();
	var formSerialized = formSerializeArray($('#'+formId));
	var inputData = {
			"atg-rest-depth": "4",
			"arg1":formSerialized,
			
			"arg2":headerFieldsSerialized
	};
	
	$.ajax({
		type: 'POST',
		url : '/rest/bean/com/charter/tools/facade/ToolsServiceFacade/insertLead',
		contentType : 'application/json',
		async       : false,        
		data        : JSON.stringify(inputData),
        success: function(data) {
    		if (data.error) {
    			if (displayValidationError == undefined || displayValidationError == true) {
    				FORM.showValidationErrors(data.validationErrors, LEAD.validationMessages());
    			}
    			FORM.showFormResultErrors(data.resultErrorTO);
    			if ($.isFunction(errorFunction)) {
    				errorFunction(data);
    			}
    		} else {
    			successFunction(data);
    		}
        },
		error		: ajaxErrorFunction
	});
	
	
	
};

LEAD.validationMessages = function() {
	var validationMsgsMap = {};
	validationMsgsMap['required'] = 'This field is required.';
	validationMsgsMap['validateEmail'] = 'Please enter a valid email address.';
	validationMsgsMap['validatePhone'] = 'Please enter a valid phone number';
	validationMsgsMap['validateZipCode'] = 'Please enter a valid zip code';
	validationMsgsMap['validateId'] = 'Please enter a valid agent Id';
	
	return validationMsgsMap;
};
