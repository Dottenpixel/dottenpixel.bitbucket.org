//install cmd: npm install gulp browser-sync gulp-less gulp-minify-css gulp-rename gulp-debug gulp-sourcemaps gulp-plumber gulp-autoprefixer --save-dev

var gulp        	= require('gulp');
var browserSync 	= require('browser-sync');
var reload      	= browserSync.reload;
var less        	= require('gulp-less');
var minifyCSS   	= require('gulp-minify-css');
var rename      	= require('gulp-rename');
var debug			= require('gulp-debug');
var sourcemaps		= require('gulp-sourcemaps');
var plumber     	= require('gulp-plumber');
var autoprefixer	= require('gulp-autoprefixer');

var lessPath = './app/resources/css/';
var campaignName = 'spectrum-campaign-q2-2015';
var buildPathPage = './build/page/';
var buildPathAssets = './build/mediacontent/'+campaignName+'/';

function errorHandler (error) {
  console.log(error.toString());
  this.emit('end');
}

// start server
gulp.task('browser-sync', function() {
  browserSync({
		server: {
		  baseDir: "./build/"
		},
		port: 3002
  });
});

gulp.task('less', function() {
	return gulp.src([lessPath + 'lp-main.less'])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(less().on('error', errorHandler))
		.pipe(autoprefixer({ "browsers" : ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Safari >= 6', 'Explorer >= 8'] }))
		.pipe(plumber.stop())
		.pipe(rename('styles.css'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(buildPathAssets + "css/"))
})

gulp.task('css', function(){
	console.log("CSS file ready");
	return gulp.src(buildPathAssets + "css/styles.css/")
		.pipe(plumber())
		.pipe(minifyCSS())
		.pipe(plumber.stop())
		.pipe(rename('styles.min.css'))
		.pipe(gulp.dest(buildPathAssets + "css/"))
})

// process JS files and return the stream.
gulp.task('js', function () {
	return gulp.src('./app/resources/js/*js')
		.pipe(gulp.dest(buildPathAssets + "js/"))
});

// process HTML files and return the stream.
gulp.task('html', function () {
	return gulp.src('./app/html/*.html')
		.pipe(gulp.dest(buildPathPage))
});

// process aync files and return the stream.
gulp.task('async', function () {
	return gulp.src('./app/resources/async/*.*')
		.pipe(gulp.dest(buildPathAssets + "async/"))
});

// process image files and return the stream.
gulp.task('imgs', function () {
	return gulp.src('./app/resources/images/*.*')
		.pipe(gulp.dest(buildPathAssets + "img/"))
});
gulp.task('ie-imgs', function () {
	return gulp.src('./app/resources/images/svg-replace/*.*')
		.pipe(gulp.dest(buildPathAssets + "img/svg-replace/"))
});

// Reload all Browsers
gulp.task('bs-reload', function () {
	reload();
});

gulp.task('default', ['browser-sync'], function() {
	gulp.watch([lessPath+"*.less"], ['less']);
	gulp.watch(['./app/html/*.html', './app/resources/**/*'], ['imgs', 'ie-imgs', 'async', 'html', 'css', 'js', 'bs-reload']);
})

