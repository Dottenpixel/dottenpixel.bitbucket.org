function SpectrumCampaignLPQ12015() {
	
	function enableSmoothAnchorScroll() {
		$('a[href*=#]:not([href=#])').click(function() {
			console.log("row-header", $(".row-header").height());
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				var animateScroll = function(options) {
					$('html,body').animate({
						scrollTop: target.offset().top - $(".row-header").height() - $("nav.module-nav").height()
					}, options);
				} 
				var onAnimationComplete = function(){
					console.log("ending target.offset().top", target.offset().top - $(".row-header").height());
					//adjust for moving header and nav during animation
					animateScroll({
						"duration" : 600
					});
				}			
				if (target.length) {
					console.log("starting target.offset().top", target.offset().top);
					animateScroll({
						"duration" : 1000,
						"complete" : onAnimationComplete
					});
					return false;
				}
			}
		});
	}

	function attachScrollListeners() {
		var checkWaypoints = function(e) {
			$(".scroll-module:not('.introduced')").each(function(i,o) {
				var $module = $(o);
				var waypoint = $module.offset().top - $(window).height()*.44;
				//console.log(waypoint, "is a waypoint");
				if ( $(window).scrollTop() > waypoint ) {
					console.log($module, "is being introduced");
					$module
						.addClass("introduced")
						.trigger("introduce");
				}
			})
		}
		$(window).on("scroll", checkWaypoints);
	}

	function scrollDebug() {
		$(window).on("scroll", function(e){
			console.log("scrollTop:", $(window).scrollTop());
		});
	}

	function enableRevealPanels() {
		$(".reveal-panel").on("click", ".reveal-trigger", function(e){
			e.preventDefault();
			$(e.delegateTarget).addClass("revealed");
		})
	}

	function makeSticky() {
		var oWidthForm, oWidthHeader, oWidthNav, oTopNav;
		var resetWidths = function(e) {
			$(".form-col-wrapper, .row-header, nav.module-nav").css({ "width" : "" });
			oWidthForm = $(".form-col-wrapper").width();
			oWidthHeader = $("#container-main").outerWidth();
			oWidthNav = $("nav.module-nav").width();
			oTopNav = $("nav.module-nav").offset().top - $("nav.module-nav").height();
		}
		var checkStickiness = function(e) {
			//form
			var $formCol = $(".col-aside");
			var waypoint = $formCol.offset().top;
			$formCol.toggleClass("sticky", $(window).scrollTop() > waypoint);
			$(".form-col-wrapper").css({ "width" : oWidthForm });
			//header
			$(".row-header")
				.toggleClass("sticky", $(window).scrollTop() > $(".row-header").height())
				.css({ "width" : oWidthHeader });
			//nav
			$("nav.module-nav")
				.toggleClass("sticky", $(window).scrollTop() > oTopNav)
				.css({ "width" : oWidthNav });
		}
		var resetStickiness = function() {
			console.log("resetting stickiness");
			resetWidths();
			checkStickiness();
		};
		
		$(window).on("scroll", checkStickiness);
		$(window).on("resize", resetStickiness);
		resetWidths();
	}

	function introTop() {
		setTimeout(function(){
			$(".intro-module").addClass("introduced");
		},
		2000);
		
	}
	this.loadSVG = function() {
		loadSVG.apply(this, arguments);
	}

	function handleResize() {
		$(window).on("resize", 
			function(){
				console.log("resize");
			}
		);
	}

	function handleFormPlacement() {
		//if the user is on a xs breakpoint, move form to main column
		if(window.matchMedia("(max-width: 767px)").matches) {
			$("#mobile-form-container").append($(".form-col-wrapper"));
		}
	}

	function handlePseudos() {
		$("nav li:last-child").addClass("last-child");
	}

	function handleScrollModules() {
		$(".scroll-module").addClass("introduced");
	}
	
	function handlePhoneFeaturesIntro() {
		$(".new-phone-features-module").on("introduce", function(e){
			$(".phone-feature-meter-area").each(function(i,o) {
				var duration = 90;
				var intervalFunction = function() {
					$(o).addClass("introduced");
					var iconInterval = setInterval(function(){
						var $well = $(".phone-feature-meter-well", o);
						var count = parseInt($well.data("iconCount"));
						var $icon = $(".phone-meter-icon:eq(0)", $well).clone();
						var $wrapper = $(".icon-wrapper", $well);
						$wrapper.append($icon);
						setTimeout(function(){
							$icon.addClass("intro")
						}, 50);
						if( $(".phone-meter-icon", $well).length >= count ) {
							clearInterval(iconInterval);
						}
					}, duration)
				}
				setTimeout(intervalFunction, ($(o).is(".spectrum") ? 1 : parseInt($(".spectrum .phone-feature-meter-well").data("iconCount"))*duration));
			})
		})
	}

	$(document).ready(
		function() {
			if($("html").is(".ie8")) {
				handlePseudos();
				handleScrollModules();
			} else {
				handleResize();
				handleFormPlacement();
				enableSmoothAnchorScroll();
				scrollDebug();
				attachScrollListeners();
				makeSticky();
				introTop();
			}
			enableRevealPanels();
			handlePhoneFeaturesIntro();
		}
	);
}

function getPageParams() {
	// get params from targeted page
	var params = defaultParams();
	
	var url_vars = $.deparam.querystring();
	var leadRef="", start="";
	if (!$.isEmptyObject(url_vars)) {
		if (url_vars.R) {
			params.region = url_vars.R.toUpperCase();
		}
		if (url_vars.V) {
			params.variant = url_vars.V.toUpperCase();
		}
		if (url_vars.test) {
			params.test = url_vars.test;
		}	
		if (url_vars.start) {
			if (url_vars.start!= '0') {
				params.start = true;
			}
		}
		if (url_vars.ver) {
			params.ver = url_vars.ver.toLowerCase();
		}		
		if (url_vars.leadRef) {
			leadRef = url_vars.leadRef.toLowerCase();
		}

	}
	// * urls have been trafficked without a creative variant indicated
	//    so force it here based on leadRef or ver
	if (leadRef.indexOf('LP_EM_speedchallenge'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_CooperSmith_Print_SPD_CHL'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_CooperSmith_Radio_SPD_CHL'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_DM_Speed'.toLowerCase()) >= 0 ||
			params.ver == "sg" ||
			params.ver == "scn" )
	{
				params.variant = "DIG";
	}
	
	if (params.region==="CS") {
		params.speed = "200";
	} else if (params.region==="MS") {
		params.speed = "60";
		if (params.variant==="DIG") {
			// no gift card offer for central states
			params.variant = "DIGNO";
		}
	} else {
		params.region = "GEN";
		params.speed = "100";
	}
	//console.log(params);
	return params;
}

var _app = new SpectrumCampaignLPQ12015();