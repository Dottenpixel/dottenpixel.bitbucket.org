/*jslint browser: true*/
/*global $, jQuery, alert*/
var speedTest = {
		'path' : '/mediacontent/speedtest/blue/',
		'rootPath' : '/mediacontent/speedtest/',
		'usrTimeout' : null,
		'swfLoaded' : false,
		'params' : {
				region: "GEN",
				speed: "100",
				variant: "DIG",
				test: "",
				ver : "",
				start : false
		},
		'speedVals' : {
				'100': {
					name : 'Internet Pro100',
					userMeter: 'img/speedometer-user.png',
					charterMeter: 'img/speedometer-cb.png',
					angleFunc : mbpsToAngle,
					ulAngleFunc: mbpsToAngle10,
					intervalLow: 100,
					intervalHigh: 106,
					ulIntervalLow: 7,
					ulIntervalHigh: 8,
					pdfText : '2 seconds',
					videoText : '16 seconds',
					softwareText : '50 seconds',
					overBadge : 'img/over100.png'
				},
				'200': {
					name : 'Internet Pro200',
					userMeter: 'img/speedometer-user.png',
					charterMeter: 'img/speedometer-cb.png',								
					angleFunc : mbpsToAngle200,
					ulAngleFunc: mbpsToAngle10,					
					intervalLow: 200,
					intervalHigh: 205,
					ulIntervalLow: 7,
					ulIntervalHigh: 8,					
					pdfText : '1 second',
					videoText : '8 seconds',
					softwareText : '20 seconds',
					overBadge : 'img/over200.png'
				},
				'30': {
					name : 'Internet Essentials30',
					userMeter: 'img/speedometer-user.png',
					charterMeter: 'img/speedometer-cb.png',	
					angleFunc : mbpsToAngle30,
					ulAngleFunc: mbpsToAngle6,					
					intervalLow: 30,
					intervalHigh: 35,
					ulIntervalLow: 4,
					ulIntervalHigh: 5,					
					pdfText : '8 seconds',
					videoText : '55 seconds',
					softwareText : '2 minutes and 13 seconds',
					overBadge : 'img/over30.png'
				},
				'60': {
					name : 'Internet Essentials60',
					userMeter: 'img/speedometer-user.png',
					charterMeter: 'img/speedometer-cb.png',	
					angleFunc : mbpsToAngle60,
					ulAngleFunc: mbpsToAngle6,					
					intervalLow: 60.1,
					intervalHigh: 62.1,
					ulIntervalLow: 4.1,
					ulIntervalHigh: 4.5,					
					pdfText : '4 seconds',
					videoText : '28 seconds',
					softwareText : '1 minute and 7 seconds',
					overBadge : 'img/over60.png'
				}							
		},
		"getVals" : function () {
			return speedTest.speedVals[speedTest.params.speed];
		},
    el : {
        body: $("body"),
        btnStart : $('#startTest'),
        btnRestart : $('#restartTest'),
        resultsInner : $('.results'),
        resultsFast : $('#resultsFast'),
        resultsSlow : $('#resultsSlow'),
        disclaimer : "",
        ie8 : false,
        noFlash : "Thank you for visiting speedtest.charter.com. To view this site, please download and install Adobe Flash Player or update your current version <a href='http://get.adobe.com/flashplayer/' target='_blank'>here.</a>"
    },
    'speeds' : {
        userDL : "",
        userUL : "",
        cbDL : 0,
        cbUL : randomIntFromInterval(7,7).toFixed(2)     
    },
    'speedsSetDefault': function() {
				speedTest.speeds.cbDL = randomNumFromInterval( 
																	speedTest.getVals().intervalLow, 
																	speedTest.getVals().intervalHigh ).toFixed(2);
				//console.log('speedTest.speeds.cbDL ', speedTest.speeds.cbDL );
    },
    'hasFlash' : function () {
        var flashVersion = swfobject.getFlashPlayerVersion();
        return flashVersion.major >= 10;
    },
    'init' : function (params){
    		if (params) {
    			speedTest.params = params;
    		}
    		speedTest.speedsSetDefault();
        speedTest.setupCanvas();
        speedTest.updateCopy();
        speedTest.el.btnRestart.hide();

        speedTest.el.resultsInner.hide();
        speedTest.el.resultsFast.hide();
        speedTest.el.resultsSlow.hide();
				/* test
        speedTest.el.resultsInner.show();
        speedTest.el.resultsFast.hide();
        speedTest.el.resultsSlow.show(); 
        speedTest.userFastForm();
				*/
        
        if (speedTest.hasFlash() == true) {
            speedTest.setupOokla();
            this.bindUIActions();
        } 
        // auto start logic 
        if (speedTest.params.start) {
        	speedTest.autoStart();
        } else {
					// fake the user needle for the opening
					speedTest.fakeUser();        
        	speedTest.showWelcome();
        }
        // no flash 
        if (speedTest.hasFlash() != true) {
        	speedTest.noFlash();
        }
        // load some imgs ahead of time
        speedTest.preloadImages();
    },
    'showWelcome' : function() {
    	if (speedTest.el.ie8 == true ) {
        $('#welcome').css('display', 'block');
      } else {
      	$('#welcome').fadeIn(100);
      }
    },
    'autoStart' : function() {
    		// logic is on click event
    		speedTest.el.btnStart.click();
    },
    'userFastForm' : function() {
    	// move charter form into correct element
    	$("#formwrap").detach().appendTo('#rff-form-target');  
    	
    	$('#formwrap #insertLeadBtn').addClass('button3');
    	$('#formwrap #insertLeadBtn').html('Learn More');
    	// button color test
    	if (btnColorTest) {
    		btnColorTest.updateBtnTestTracking('learnmore')
    	}
    },
    'updateCopy' : function() {
    	// update copy based on the region speed
    	//$('.serviceSpeed').html(speedTest.params.speed);
    	//$('.serviceName').html(speedTest.getVals().name);
    	
    	// update copy based on the offer
    	if (speedTest.params.variant == "DIGNO") {
    		$('#welcome-offer-2').show();
    		$('#welcome-offer-1').hide();
    		$('#rs-offer-2').show();
    		$('#rs-offer-1').hide();    		
    		$('#rf-offer-2').show();
    		$('#rf-offer-1').hide();     
    		$('#disclaimer-2').show();
    		$('#disclaimer-1').hide(); 
    		    		   		
    	} else if (speedTest.params.variant == "DIG") {
    		$('#welcome-offer-1').css('visibility', 'visible');
    		$('#rf-instructions').show();
    	
    	} else if (speedTest.params.variant == "TV") {
    		$('#welcome-offer-2').hide();
    		$('#welcome-offer-1').hide();    		
    		$('#disclaimer-2').show();
    		$('#disclaimer-1').hide();    		
    		$('#welcome-button').addClass('welcome-button-up');
    	}
    	
    	// show proper disclaimer 
    	$('#disclaimer-'+ speedTest.params.region + '-' + 
    			(speedTest.params.variant == "DIG" ? "GC" : "NO")).addClass('disclaimer-copy-visible');
    	
    },
    'noFlash' : function () {
    	//console.log('noFlash!');
    	
    	$('#welcome').css('display', 'none');
    	$('#welcome-no-flash').css('display', 'block');
    	
    	// make sure cb speedo elements are hidden
    	$('#chartergauge').hide();
    	$('#cbspeed, #usrspeed, #usrmbps, #cbmbps').hide();
    	$('#statusLine1, #statusLine2, #statusLine3').hide();
    	$('#currentSpeed1, #currentSpeed2, #charterSpeed1, #charterSpeed2').hide();
    	
			$('.results').show();
			$('#resultsFast').show();
			$('#resultsSlow').hide();
			// move charter form in the dom
			speedTest.userFastForm();
			$('.formCharter').show();   	
			
			// manipulate the resultsFast area
			$('#resultsFast').addClass('results-noflash');
			$('#resultsFast-copy').hide();
			$('.rf-offer').hide();
			$('#rf-instructions').hide();
			$('#rff-formbox-heading').hide();
			$('#rff-formbox-heading-noflash').show();
			$('#rsf-message-noflash').show();
			$('#insertLeadBtn').html('Switch Today');
			// button color test
    	if (btnColorTest) {
    		btnColorTest.updateBtnTestTracking('switchtoday')
    	}
			
			//add the correct url
			if (speedTest.params.ver=='sg') {
				$('.welcome-no-flash-url').html('SpeedGuarantee.com');
			} else if (speedTest.params.ver=='scn') {
				$('.welcome-no-flash-url').html('SpeedCheckNow.com');
			}
			
    },    
    'bindUIActions':function (){
        speedTest.el.btnStart.on("click", function(){

				// make sure opening animation is done
				speedTest.fakeUserEnd();	

        if (speedTest.el.ie8 == true ) {
            $('#startTest').css('display', 'none');
            $('#welcome').css('display', 'none');
            //$('#disclaimer').show();
            $('#chartergauge').velocity("fadeIn", { duration: 500 });
            $('#usrmbps').velocity("fadeIn", { duration: 800 });
            $('#cbmbps').velocity("fadeIn", { duration: 800 });
            $('#cbspeed').velocity("fadeIn", { duration: 800 });
            $('#usrspeed').velocity("fadeIn", { duration: 800 });

            $("#statusLine1").velocity("fadeIn", { delay: 500, duration: 500 });
            $("#statusLine2").velocity("fadeIn", { delay: 500, duration: 500 });
            $("#statusLine3").velocity("fadeIn", { delay: 500, duration: 500 });
            $('#icon-progress-bg').velocity("fadeIn", { duration: 800 });
            $('#icon-network').velocity("fadeIn", { duration: 800 });
            $('#icon-user').velocity("fadeIn", { duration: 800 });
            $('#progress-dots').velocity("fadeIn", { duration: 800 });

            $('#currentSpeed1').velocity("fadeIn", { delay: 500, duration: 500 });
            $('#currentSpeed2').velocity("fadeIn", { delay: 500, duration: 500 });
            $('#charterSpeed1').velocity("fadeIn", { delay: 1000, duration: 500 });
            $('#charterSpeed2').velocity("fadeIn", { delay: 1000, duration: 500 });

        } else {
            animationsOne().then(animationsTwo());
            animationsTwo().then (startTesting());
        }
            function animationsOne (){
                var deferred = $.Deferred();
                speedTest.el.btnStart.fadeOut();
                $('#welcome').fadeOut();
                $('#chartergauge').fadeIn(500);
                //$('#disclaimer').show();
                $('#cbspeed, #usrspeed, #usrmbps, #cbmbps').show();
                $('#statusLine1, #statusLine2, #statusLine3').fadeIn(800);
                $('#icon-progress-bg, #icon-network, #icon-user, #progress-dots').fadeIn(800);
                return deferred.promise();
            }
            function animationsTwo (){
                var deferred = $.Deferred();
                $('#currentSpeed1, #currentSpeed2').delay(500).fadeIn(500);
                $('#charterSpeed1, #charterSpeed2').delay(1000).fadeIn(500);
                return deferred.promise();
            }
            function startTesting () {
                setTimeout(function() { /* speedTest.startSpeedTest(); */ }, 3000);
            }
            speedTest.startSpeedTest();
        });
        speedTest.el.btnRestart.on("click", function(){
            speedTest.restartTest();
        });
    },
    'setupCanvas' : function (){
        //Raphael Canvas
        userPaper = Raphael("user-canvas", 960, 500);
        charterPaper = Raphael("charter-canvas", 960, 500);
        resultsPaper = Raphael("results-canvas", 960, 500);

        userPaper.canvas.setAttribute('id', 'usergauge');
        charterPaper.canvas.setAttribute('id', 'chartergauge');
        resultsPaper.canvas.setAttribute('id', 'resultspaper');

        // Create gauge objects (min angle, max angle)
        // 0 = right
        usrGauge = userPaper.gauge(-53, 171);
        cbGauge = charterPaper.gauge(-53, 171);

        // Gauge backgrounds xpos,ypos,width,height,center x,center y
        usrGauge.bg(userPaper.image(speedTest.path + speedTest.getVals().userMeter, 31, 68, 358, 373), [211, 234]);
        cbGauge.bg(charterPaper.image(speedTest.path + speedTest.getVals().charterMeter, 570, 68, 358, 373), [750, 234]);  			


        // Gauge Needles xpos,ypos,width,height,center x,center y
        userNeedle = userPaper.image(speedTest.path + "img/needle2.png", 0, 0, 135, 41);
        userNeedle.node.id = 'user-needle';
        cbNeedle = charterPaper.image(speedTest.path + "img/needle2.png", 0, 0, 135, 41);
        cbNeedle.node.id = 'cb-needle';
        
        usrGauge.pointer(userNeedle, [120,21]);      
        cbGauge.pointer(cbNeedle, [120,21]);
        
        // Start point of needle
        usrGauge.move(0, false);
        cbGauge.move(0, false);

        //Speedometer Labels
        if (speedTest.el.ie8 == true) {
        	// 211 750
        	usrspeed = charterPaper.text(171, 302, "0");
        	userMbps = userPaper.text (251, 306,"Mbps");
					charterSpeed = charterPaper.text (710, 302,"0");
					charterMbps = resultsPaper.text (790, 306,"Mbps");        	
        } else { 
					usrspeed = charterPaper.text(178, 302, "0");
					userMbps = userPaper.text (218, 302,"Mbps");
								
					charterSpeed = charterPaper.text (717, 302,"0");
					charterMbps = resultsPaper.text (758, 302,"Mbps");
				}
        usrspeed.node.id = 'usrspeed';
        charterSpeed.node.id = 'cbspeed';
        userMbps.node.id = 'usrmbps';
        charterMbps.node.id = 'cbmbps';

        //Labels
        currentSpeed1 = resultsPaper.text(211,463,"Your Current");
        currentSpeed2 = resultsPaper.text(211,491,"Internet Speed");
        currentSpeed1.node.id = 'currentSpeed1';
        currentSpeed2.node.id = 'currentSpeed2';

        charterSpeed1 = resultsPaper.text(750,463,"What's Possible With");
        charterSpeed2 = resultsPaper.text(750,491,"Charter Business");
        charterSpeed1.node.id = 'charterSpeed1';
        charterSpeed2.node.id = 'charterSpeed2';

        //Since there is no easy way
        // of adding breaks to SVG paths
        // I manually created multiple lines
        statusLine1 = userPaper.text (480,167,"ESTABLISHING");
        statusLine2 = userPaper.text (480,195,"CONNECTION");
        statusLine3 = userPaper.text (480,223,"");

        statusLine1.node.id = 'statusLine1';
        statusLine2.node.id = 'statusLine2';
        statusLine3.node.id = 'statusLine3';
        
        testingLine1 = userPaper.text (480,210,"");
        testingLine2 = userPaper.text (480,247,"");
        testingLine3 = userPaper.text (480,284,"");                
        
        testingLine1.node.id = 'testingLine1';
        testingLine2.node.id = 'testingLine2';
        testingLine3.node.id = 'testingLine3';  
        
        completeLine1 = userPaper.text (480,89,"");
        completeLine2 = userPaper.text (480,124,"");
        completeLine1.node.id = 'completeLine1';
        completeLine2.node.id = 'completeLine2';
                      

        if (speedTest.el.ie8 == true) {
            usrspeed.attr({fill: '#fff', 'font-size': 36});
            charterSpeed.attr({fill: '#fff', 'font-size': 36});
            userMbps.attr({fill: '#fff', 'font-size': 22});
            charterMbps.attr({fill: '#fff', 'font-size': 22});

            currentSpeed1.attr({fill: '#0072ae', 'font-size': 25});
            currentSpeed2.attr({fill: '#0072ae', 'font-size': 25});

            charterSpeed1.attr({fill: '#0072ae', 'font-size': 25});
            charterSpeed2.attr({fill: '#0072ae', 'font-size': 25});

            statusLine1.attr({fill: '#ffffff', 'font-size': 22});
            statusLine2.attr({fill: '#ffffff', 'font-size': 22});
            statusLine3.attr({fill: '#ffffff', 'font-size': 22});
            
           	testingLine1.attr({fill: '#ffffff', 'font-size': 28});
            testingLine2.attr({fill: '#ffffff', 'font-size': 28});
            testingLine3.attr({fill: '#ffffff', 'font-size': 28}); 
            
            completeLine1.attr({fill: '#ffffff', 'font-size': 28}); 
            completeLine2.attr({fill: '#ffffff', 'font-size': 28});            
        }
        
        icon_progress_bg = resultsPaper.image(speedTest.path + "img/top-bg.png", 340, 0, 279, 98);
        icon_progress_bg.node.id = 'icon-progress-bg';

        icon_network = resultsPaper.image(speedTest.path + "img/internet.png", 364, 31, 55, 55);
        icon_user = resultsPaper.image(speedTest.path + "img/computer.png", 536, 36, 59, 47);
        icon_network.node.id = 'icon-network';
        icon_user.node.id = 'icon-user';
        
        progress_dots = resultsPaper.image(speedTest.path + "img/progress0.png", 441,52,73,15);
        progress_dots.node.id = 'progress-dots';

        download_arrow = resultsPaper.image(speedTest.path + "img/download.png", 449,107,62,62);
        upload_arrow = resultsPaper.image(speedTest.path + "img/upload.png",  449,107,62,62);

        download_arrow.node.id = 'download-arrow';
        upload_arrow.node.id = 'upload-arrow';
        
        if (speedTest.el.ie8 == true) {
					// NOTE: raphaeljs rendering images offset and repeating on IE8
					//   apparently a known issue
					// this makes things less ugly at least
        	icon_network.scale(.95, .95);
        	icon_user.scale(.95, .95);
        	download_arrow.scale(.8, .8);
        	upload_arrow.scale(.8, .8);
        }
        
				banner_done = resultsPaper.image(speedTest.path + "img/increase.png", 388, 157, 183, 108);
				banner_done.node.id = 'banner-done';
				banner_done.hide();  
				
				banner_over = resultsPaper.image(speedTest.path + speedTest.getVals().overBadge, 388, 157, 183, 164);
				banner_over.node.id = 'banner-over';
				banner_over.hide();

        $('#cbspeed, #usrspeed, #usrmbps, #cbmbps').hide();
        $('#progress-dots').hide();
        $('#icon-progress-bg, #icon-network, #icon-user').hide();
        $('#statusLine1, #statusLine2, #statusLine3').hide();
        $('#currentSpeed1, #currentSpeed2, #charterSpeed1, #charterSpeed2').hide();
        $('#download-arrow, #upload-arrow').hide();
        $('#chartergauge').hide();


    },
    'fakeDL' : function (){
        speedTest.updateCBPointer(0);
        //var randomAngle = randomIntFromInterval( speedTest.getVals().intervalLow , speedTest.getVals().intervalHigh );
        speedTest.updateCBPointer(randomNumFromInterval(95,99));

    },
    'fakeUL' : function (){
        speedTest.updateCBPointer(0);
        /*
        var randomAngle = randomIntFromInterval(0,22.5);
        var percent2 = angleToPercent(randomAngle, -53, 171);
        speedTest.updateCBPointer(percent2);
				*/
				// put it up in the red zone
				speedTest.updateCBPointer(randomNumFromInterval(85,89));
    },
    'fakeUser' : function() {
    	speedTest.usrTimeout = window.setTimeout( function() {
				speedTest.updateUserPointer(0);
				speedTest.updateUserPointer(randomNumFromInterval(95,99));
				speedTest.fakeUserWobble();   	
    	}, 2500);
    	// kill after a couple minutes to save people's batteries
    	window.setTimeout( function() {
    		window.clearTimeout(speedTest.usrTimeout);
    	}, 2*60*1000);
    },
    'fakeUserWobble' : function() {
    	speedTest.usrTimeout = window.setTimeout( function() {
    		speedTest.updateUserPointer(randomNumFromInterval(85,96));
    		speedTest.fakeUserWobble();
    	}, 400);
    },
    'fakeUserEnd' : function() {
    	window.clearTimeout(speedTest.usrTimeout);
    	speedTest.updateUserPointer(0);
    },
    'updatePointer' : function(percent){
			speedTest.updateUserPointer(percent);
    },
    'updateUserPointer' : function(percent) {
    		// replace with glow if necessary
    		userNeedle.attr('src', speedTest.pointerImage(percent) );
    		//console.log(percent, userNeedle.attr('src'));
        usrGauge.move(percent);    
    },
    'updateCBPointer' : function(percent) {
    		// replace with glow if necessary
    		cbNeedle.attr('src', speedTest.pointerImage(percent) );
        cbGauge.move(percent);       
    },
    'pointerImage' : function (percent) {
    	var pImg = speedTest.path +'img/needle2.png';
    	if (percent > 72) {
    		pImg = speedTest.path +'img/needle2-glow.png';
    	}
    	return pImg
    }, 
    'setupOokla' : function(){
        var flashvars = {
            configExtension: "jsp"
        };
        var params = {
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "always"
        };
        var attributes = {
            id: "speedtestobj"
        };
        //swfobject.embedSWF(engine.swf URL, id of parent div, width, height, version, )
        swfobject.embedSWF(speedTest.rootPath + "engine.swf?v=3.0", "speedtest", "1", "1", "8.0.0", speedTest.path + "expressInstall.swf", flashvars, params, attributes, swfCallback);

    },
    'startSpeedTest': function (){
    		speedTest.fakeUserEnd();
    		
    		// activate test mode
    		if (speedTest.params.test) {
    			speedTest.testMode();
    			return;
    		}
    		
        var speed = document.getElementById("speedtestobj");
        //console.log(speed);
        if(speed && speed.startTest) {
        	speedTest.swfLoaded = true;
        	speed.startTest(1); // 1 = serverid
        } else {
        
        	// flash object is still loading - poll
        	var n = 0;
        	var checkInterval = window.setInterval(function() {
        		//console.log('checking', n);
        		if (speedTest.swfLoaded) {
        			speed = document.getElementById("speedtestobj");
        			//console.log('swfLoaded', speed);
        			if (speed && speed.startTest) {
        				window.clearInterval(checkInterval);
        				speed.startTest(1); // 1 = serverid
        			}
        		}
        		n++;
        		if (n > 200) {
        			console.log('Problem: Flash object not loaded!');
        			window.clearInterval(checkInterval);

        			speedTest.noFlash();
        		}
        	}, 100);
        	
        }
    },
    'testMode' : function() {
    	/** simulate speed test **/
    	console.log('testMode: mode '+ speedTest.params.test);
    	
			speedTest.params.testDL = 1258900;
			speedTest.params.testUL = 125230;
    	if (speedTest.params.test=='2') {
				speedTest.params.testDL = 14589000;
				speedTest.params.testUL = 1252300;
    	}
    	if (speedTest.params.speed=="200") {
    		speedTest.params.testDL *= 2;
    		speedTest.params.testUL *= 2;
    	} else if (speedTest.params.speed=="30") {
    		speedTest.params.testDL *= 0.3;
    		speedTest.params.testUL *= 0.3;    		
    	} else if (speedTest.params.speed=="60") {
    		speedTest.params.testDL *= 0.6;
    		speedTest.params.testUL *= 0.6;    		
    	}    	
    	if (speedTest.params.test=='0') {
    		console.log('testMode: doing nothing');
    		return;
    	}
    	
    	var delay = 5 * 1000;
    	//var delay = 2 * 1000;
    	window.setTimeout( function() {
    		console.log('testMode: connecting');
    		window.setTimeout( function() {
    			console.log('testMode: download progress');
    			speedTest.updateProgress('download', speedTest.params.testDL, .50);
    			window.setTimeout( function() {
    				console.log('testMode: upload progress');
    				speedTest.updateProgress('upload', speedTest.params.testUL, .50);
    				window.setTimeout( function() {
    					console.log('testMode: finished');
    					speedTest.testFinished('global', 20);
    				}, delay);
    			}, delay);	
    		}, delay);
    	}, delay/2);
    
    },
    'updateProgress' : function (test, result, progress){
        var testPercentDone = Math.floor(progress*100);
        var curResult = Math.round(result/12500)/10;
				var angle = 0;
				if (test == 'upload') {
					// dynamically get upload mbpsToAngle function
					angle = speedTest.getVals().ulAngleFunc(curResult);
				} else {
					// dynamically get mbpsToAngle function
					angle = speedTest.getVals().angleFunc(curResult);
				}        
        var minAngle = angle[0];
        var maxAngle = angle[1];
        var randomAngle = randomIntFromInterval(minAngle,maxAngle);
        // sets User gauge angle
        var percent = angleToPercent(randomAngle, -53, 171);
        
        // Get animation frame 
				// 0 1 2 3 4 5 
				// 1 2 3 1 2 3 
				var numImgs = 3
				var numFrames = 6;
				var frame = (Math.floor((testPercentDone-1)/(100/numFrames)) % numImgs) +1;        
        
        if (test == 'latency') {
            //Establishing Connection
        }
        if(test == 'download'){
						// move pointers
            speedTest.fakeDL();
            speedTest.updatePointer(percent);

						// CB Speed
            var randomDL = randomNumFromInterval( 
														speedTest.getVals().intervalLow, 
														speedTest.getVals().intervalHigh ).toFixed(2);
            
            if (speedTest.el.ie8 == true){
                statusLine1.attr({'text':" " });
                statusLine2.attr({'text': " " });
                statusLine3.attr({'text':" " });
                testingLine1.attr({'text':"TESTING" });
                testingLine2.attr({'text': "DOWNLOAD" });
                testingLine3.attr({'text':"SPEED" });                

                $('#download-arrow').css('display', 'block');

                usrspeed.attr({'text':curResult });
                charterSpeed.attr({'text': randomDL });            
            } else {
                $('#statusLine1').text("");
                $('#statusLine2').text("");
                $('#statusLine3').text("");
                $('#testingLine1').text("Testing");
                $('#testingLine2').text("Download");
                $('#testingLine3').text("Speed");                
                $('#download-arrow').fadeIn(2000).delay(500).fadeOut(1500);
                
                $('#usrspeed').text(curResult);                
                $('#cbspeed').text(randomDL);

								// center labels
								speedTest.positionUsrSpeed();
								speedTest.positionCbSpeed();
                
            }
            //icon_user.attr({src: speedTest.path + "img/icons/icon_user_on.png"});
            //icon_network.attr({src: speedTest.path + "img/icons/icon_network_on.png"});
						
						var progImg = 'img/progress0.png';
						if (testPercentDone >0 && testPercentDone < 100) { 
							progImg = 'img/progress'+encodeURIComponent(frame)+'.png';
							//console.log(testPercentDone, progImg);
						} 						
						progress_dots.attr({src: speedTest.path + progImg});

        }
        if(test == 'upload'){
        		// update pointers
            speedTest.updateCBPointer(0);
            speedTest.updatePointer(percent);
            speedTest.fakeUL();
            var randomUL =(randomNumFromInterval(speedTest.getVals().ulIntervalLow,
            																			speedTest.getVals().ulIntervalHigh));
            randomUL = randomUL.toFixed(2);
            if (speedTest.el.ie8 == true){
                testingLine2.attr({'text': 'UPLOAD' });
                $('#download-arrow').css('display', "none");
                $('#upload-arrow').css('display', 'block');

                usrspeed.attr({'text':curResult });
                charterSpeed.attr({'text': randomUL });

            } else {
                $('#testingLine2').text("Upload");
                $('#download-arrow').hide();
                $('#upload-arrow').fadeIn(2000).delay(500).fadeOut(1500);
                
								$('#usrspeed').text(curResult);                
                $('#cbspeed').text(randomUL);
								// center labels
								speedTest.positionUsrSpeed();
								speedTest.positionCbSpeed();								             
            }

						var progImg = 'img/progress-up0.png';
						/*
						if (testPercentDone >0 && testPercentDone <= 33) { 
							progImg = 'img/progress-up1.png';
						} else if (testPercentDone >33 && testPercentDone <= 66) {  
							progImg = 'img/progress-up2.png';
						} else if (testPercentDone >66 && testPercentDone <= 100) { 
							progImg = 'img/progress-up3.png';
						}
						*/
						if (testPercentDone >0 && testPercentDone < 100) { 
							progImg = 'img/progress-up'+encodeURIComponent(frame)+'.png';
							//console.log(testPercentDone, progImg);
						} 							
						progress_dots.attr({src: speedTest.path + progImg});

        }
    },
    'positionUsrSpeed' : function() {
    	speedTest.positionSpeed(211, '#usrspeed', '#usrmbps');
    },
    'positionCbSpeed' : function() {
    	speedTest.positionSpeed(750, '#cbspeed', '#cbmbps');
    },
    'positionSpeed' : function (center, speedLabel, mbpsLabel) {
				// move speed labels so they are centered
				//var speedWidth = $(speedLabel).width();
				//var mbpsWidth = $(mbpsLabel).width();
				var speedWidth = $(speedLabel)[0].getBoundingClientRect().width;
				var mbpsWidth = $(mbpsLabel)[0].getBoundingClientRect().width;
				
				var leftSide = center - (speedWidth + mbpsWidth)/2
				var speedMiddle = leftSide + speedWidth/2;
				var mbpsMiddle = leftSide + speedWidth + mbpsWidth/2;
				
				//console.log(speedWidth, mbpsWidth, leftSide, speedMiddle, mbpsMiddle);
				// set horizontal
				$(speedLabel).attr('x', speedMiddle);
				$(mbpsLabel).attr('x', mbpsMiddle);   
				 
				// make sure vertical offset is correct in all browsers
				$(speedLabel).attr('dy', 3.5);
				$(mbpsLabel).attr('dy', 3.5);
				$(speedLabel+' TSPAN').attr('dy', 3.5);
				$(mbpsLabel+' TSPAN').attr('dy', 3.5);				
    },
    'testFinished' : function (test, result) {
        if(test == 'latency'){ }
        //Here we set the value of our global download result variable
        if(test == 'download'){
            speedTest.speeds.userDL = Math.round((result/125)/100)/10;
        }
        //Here we set the value of our global upload result variable
        if(test == 'upload'){
           speedTest.speeds.userUL = Math.round((result/125)/100)/10;
        }
        if(test == 'global'){
        		//handle test mode
        		if (speedTest.params.test) {
        			speedTest.speeds.userDL = Math.round((speedTest.params.testDL/125)/100)/10;
        			speedTest.speeds.userUL = Math.round((speedTest.params.testUL/125)/100)/10;
        			
        			console.log('testMode: speeds ', speedTest.speeds.userDL, speedTest.speeds.userUL);
        		}
            speedTest.updateUserPointer(0);
            speedTest.updateCBPointer(0);

            if (speedTest.el.ie8 == true){
                $('#cbmbps').css('display', 'none');
                $('#usrmbps').css('display', 'none');
                $('#usrspeed').css('display', 'none');
                
                testingLine1.attr({'text': ' ' });
                testingLine2.attr({'text': ' ' });
                testingLine3.attr({'text': ' ' });                

                completeLine1.attr({'text': 'TEST' });
                completeLine2.attr({'text': 'COMPLETE' });
            }else {
                $('#cbmbps').hide();
                $('#usrmbps').hide();
                $('#usrspeed').hide();
                
                $('#testingLine1').text("");
                $('#testingLine2').text("");
                $('#testingLine3').text("");                

                $('#completeLine1').text("Test");
                $('#completeLine2').text("Complete");
            }
            
						if (speedTest.el.ie8 == true){
								$('#download-arrow').css('display', "none");
								$('#upload-arrow').css('display', "none");

						} else {
								$('#download-arrow').stop(true).fadeOut();
								$('#upload-arrow').stop(true).fadeOut();
								$('#download-arrow').hide();
								$('#upload-arrow').hide();
						}
						icon_progress_bg.hide();
						progress_dots.hide();						
						icon_network.hide();
						icon_user.hide();

						speedTest.displayResults();
        }
    },
    'displayResults' : function (){
        $('#restartTest').show();
        // update charter based on current speed
        speedTest.speeds.cbDL = randomNumFromInterval( 
        													speedTest.getVals().intervalLow, 
        													speedTest.getVals().intervalHigh ).toFixed(2);
        speedTest.speeds.cbUL = randomNumFromInterval( 
        													speedTest.getVals().ulIntervalLow, 
        													speedTest.getVals().ulIntervalHigh ).toFixed(2);													
        
        var usrDLSpeed = speedTest.speeds.userDL;
        var usrULSpeed = speedTest.speeds.userUL;
        var cbDLSpeed = speedTest.speeds.cbDL;
        var cbULSpeed = speedTest.speeds.cbUL;
        //console.log('usrDLSpeed', usrDLSpeed);
        //console.log('cbDLSpeed', cbDLSpeed);
        
        var decimalFactor = speedFactor(cbDLSpeed,usrDLSpeed);
        // don't round this way as it breaks when user is slightly < CB speed
        //decimalFactor = roundHalf(decimalFactor);
       	decimalFactor = (Math.round(decimalFactor*100) / 100).toFixed(2);
 
        var decimalArr = decimalFactor.toString().split('.');
        decimalArr[0] = parseInt(decimalArr[0], 10);

        if(decimalArr[1]==0 ){
            decimalFactor = decimalArr[0];
        }
        if (decimalFactor > 1000) {
        	decimalFactor = 1000;
        } else if (decimalFactor > 5){
          decimalFactor = decimalArr[0];
        }
        //decimalFactor cutoff = 1.01 was 1.5;
        //console.log('decimalFactor3', decimalFactor);
        
        scrollPage();
        $('#rt-cb-pdf').html(speedTest.getVals().pdfText);
        $('#rt-cb-video').html(speedTest.getVals().videoText);
        $('#rt-cb-software').html(speedTest.getVals().softwareText);

        if (decimalFactor >= 1.01 ) { 
        		// CB Faster
            finalUsr = userPaper.image(speedTest.path + "img/speedometer-user-loser.png", 31, 68, 358, 373);
            finalCB = charterPaper.image(speedTest.path + "img/speedometer-cb-winner.png", 570, 68, 358, 373);   
            
            banner_done.show();
            $('.factorSpeed').html(decimalFactor);
            $('#restartTest').show();
            $('.results').show();
            $('#resultsFast').hide();
            $('#resultsSlow').show();
            $('.formCharter').show();
            
            /* old values - seem to be arbitrarily made up 
            speedTest.userDLtime(2 * decimalFactor, '#rt-your-pdf');
            speedTest.userDLtime(16 * decimalFactor, '#rt-your-video');
            speedTest.userDLtime(40 * decimalFactor, '#rt-your-software');
            */
            speedTest.userDLtime(30/(usrDLSpeed/8), '#rt-your-pdf');
            speedTest.userDLtime(200/(usrDLSpeed/8), '#rt-your-video');
            speedTest.userDLtime(500/(usrDLSpeed/8), '#rt-your-software');

        } else {
        		// user is faster
            finalUsr = userPaper.image(speedTest.path + "img/speedometer-user-winner.png", 31, 68, 358, 373);
            finalCB = charterPaper.image(speedTest.path + "img/speedometer-cb-loser.png", 570, 68, 358, 373);
            
            // remove text
            currentSpeed1.attr({'text':" " });
            currentSpeed2.attr({'text':" " });
            charterSpeed1.attr({'text':" " });
						charterSpeed2.attr({'text':" " });
						
						banner_over.show();
            $('#restartTest').show();
            $('.results').show();
            $('#resultsFast').show();
            $('#resultsSlow').hide();
            // move charter form in the dom
            speedTest.userFastForm();
            $('.formCharter').show();
        }
        //Labels
        usrDownloadLabel = resultsPaper.text(211,175,"DOWNLOAD");
        usrDownloadLabel.node.id = 'usrlabelDL';

        usrUploadLabel = resultsPaper.text(211,276,"UPLOAD");
        usrUploadLabel.node.id = 'usrlabelUL';

        cbDownloadLabel = resultsPaper.text(750,175,"DOWNLOAD");
        cbDownloadLabel.node.id = 'cblabelDL';

        cbUploadLabel = resultsPaper.text(750,276,"UPLOAD");
        cbUploadLabel.node.id = 'cblabelUL';

        usrDownloadSpeed = resultsPaper.text(211,210,usrDLSpeed + " Mbps");
        usrDownloadSpeed.node.id = 'usrDL';

        usrUploadSpeed = resultsPaper.text(211,312 ,usrULSpeed + " Mbps");
        usrUploadSpeed.node.id = 'usrUL';

        cbDownloadSpeed = resultsPaper.text(750,210,cbDLSpeed + " Mbps");
        cbDownloadSpeed.node.id = 'cbDL';

        cbUploadSpeed = resultsPaper.text(750,312,cbULSpeed + " Mbps");
        cbUploadSpeed.node.id = 'cbUL';

        if (speedTest.el.ie8 == true) {
            usrDownloadLabel.attr({fill: '#fcb033', 'font-size': 18});
            usrUploadLabel.attr({fill: '#fcb033', 'font-size': 18});
            cbDownloadLabel.attr({fill: '#fcb033', 'font-size': 18});
            cbUploadLabel.attr({fill: '#fcb033', 'font-size': 18});
            usrDownloadSpeed.attr({fill: '#fff', 'font-size': 28});
            usrUploadSpeed.attr({fill: '#fff', 'font-size': 28});
            cbDownloadSpeed.attr({fill: '#fff', 'font-size': 28});
            cbUploadSpeed.attr({fill: '#fff', 'font-size': 28});
        }

    },
    'userDLtime' : function(time, selector) {
    	var minutes, seconds;
    	time = Math.ceil(time);
    	if (time > 60) {
				minutes = Math.floor(time/60);
				seconds = Math.floor(time - minutes*60);    	
				if (minutes == 1){
						$(selector).html(minutes+" minute");
				} else {
						$(selector).html(minutes+" minutes");
				}
    	} else {
    		if (time==1) {
    			$(selector).html(time+" second");
    		} else {
    			$(selector).html(time+" seconds");
    		}
    	}
    },
    'displayError' : function(code,msg){
      console.log("Error: " + code + " msg: " + msg);
      $('#errorContent').html("Error: " + code + " msg: " + msg);
      $('#errorModal').modal('show');
    },
    'testStarted' : function (test){
    },
    'resetCanvas' : function () {
        //if needed hide labels and reset canvas
    },
    'restartTest' : function (){
        location.reload(); // Later change to reset canvas
    },
    'results' : {
        resultsDiv : ""
    },
    'submitForm' : function (userDL, userUL){
    		// apparently not used
        var userDownloadSpeed = userDL;
        var userUploadSpeed = userUL;

        $("#insertLeadBtn").submit(function(e) {
            "use strict";
            e.preventDefault();
						console.log('submit cancelled..');
            return false;
        });
    },
    'preloadImages' : function () {
    	/* load images ahead of time to prevent uglyness */
    	var images = ['img/needle2-glow.png',
    								'img/progress0.png',
    								'img/progress1.png',
    								'img/progress2.png',
    								'img/progress3.png',
    								'img/progress-up0.png',    								
    								'img/progress-up1.png',
    								'img/progress-up2.png',
    								'img/progress-up3.png', 
     								'img/speedometer-user-loser.png',
     								'img/speedometer-cb-winner.png',
     								'img/speedometer-user-winner.png',
     								'img/speedometer-cb-loser.png'];
     								
     	window.setTimeout( function() {
     		var imgs = [];
     		var img;
     		for (var i=0;i<images.length;i++) {
     			img = new Image();
     			img.src = speedTest.path + images[i];
     			/*
     			img.onload = function() {
     				console.log('image loaded', this.src);
     			}
     			*/
     			imgs.push(img);
     		}
     	}, 1);   								   								  																
    
    }
};

$(document).ready(function() { 
	isIE8(); 
	
	var params = getPageParams();
	speedTest.init(params) 
});

function getPageParams() {
	// get params from targeted page
	var params = defaultParams();
	
	var url_vars = $.deparam.querystring();
	var leadRef="", start="";
	if (!$.isEmptyObject(url_vars)) {
		if (url_vars.R) {
			params.region = url_vars.R.toUpperCase();
		}
		if (url_vars.V) {
			params.variant = url_vars.V.toUpperCase();
		}
		if (url_vars.test) {
			params.test = url_vars.test;
		}	
		if (url_vars.start) {
			if (url_vars.start!= '0') {
				params.start = true;
			}
		}
		if (url_vars.ver) {
			params.ver = url_vars.ver.toLowerCase();
		}		
		if (url_vars.leadRef) {
			leadRef = url_vars.leadRef.toLowerCase();
		}

	}
	// * urls have been trafficked without a creative variant indicated
	//    so force it here based on leadRef or ver
	if (leadRef.indexOf('LP_EM_speedchallenge'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_CooperSmith_Print_SPD_CHL'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_CooperSmith_Radio_SPD_CHL'.toLowerCase()) >= 0 ||
			leadRef.indexOf('LP_DM_Speed'.toLowerCase()) >= 0 ||
			params.ver == "sg" ||
			params.ver == "scn" )
	{
				params.variant = "DIG";
	}
	
	if (params.region==="CS") {
		params.speed = "200";
	} else if (params.region==="MS") {
		params.speed = "60";
		if (params.variant==="DIG") {
			// no gift card offer for central states
			params.variant = "DIGNO";
		}
	} else {
		params.region = "GEN";
		params.speed = "100";
	}
	//console.log(params);
	return params;
}



//Ookla Engine Global Functions
function updateProgress(test, result, progress){speedTest.updateProgress(test,result,progress)}
function testFinished(test, result){speedTest.testFinished(test,result)}
function displayError(code, msg){speedTest.displayError(code,msg)}
function testStarted(test){speedTest.testStarted(test)}
//Conversions, Math, etc
function randomIntFromInterval(min, max){
		// note 1: does not return an int
		// note 2: actually returns from min to max+1
    return (Math.random()*(max-min+1)+min);
}
// works with accurate intervals
function randomNumFromInterval(min, max) {
	return (Math.random() * (max-min)) + min;
}
function angleToPercent (angle, minAngle, maxAngle) {
    return (100*(angle - minAngle))/(maxAngle-minAngle);
}
//Custom Speedometer Mbps to Angle conversion for 100Mbps case - new meter
function mbpsToAngle (x){
    switch (true) {
        case (x < 2.5):
            return [-53, -34.33];
            break;
        case (x >= 2.5 && x < 5):
            return [-34.33, -15.67];
            break;
        case (x >= 5 && x < 10):
            return [-15.67, 3];
            break;
        case (x >= 10 && x < 15):
            return [3, 21.67];
            break;
        case (x >= 15 && x < 22.5):
            return [21.67, 40.33];
            break;
        case (x >= 22.5 && x < 30):
            return [40.33, 59.00];
            break;
        case (x >= 30 && x < 40):
            return [59, 77.67];
            break;
        case (x >= 40 && x < 50):
            return [77.67, 96.33];
            break;
        case (x >= 50 && x < 62.5):
            return [96.33, 115];
            break;
        case (x >= 62.5 && x < 75):
            return [115, 133.67];
            break;
        case (x >= 75 && x < 90):
            return [133.67, 152.33];
            break;
        case (x >= 90 && x < 105):
            return [152.33, 171];
            break;
        case (x >= 105):
            return [168, 171];
            break;
    }
}
//Custom Speedometer Mbps to Angle conversion for 200Mbps case
function mbpsToAngle200 (x){
    switch (true) {
        case (x < 5):
            return [-53, -34.33];
            break;
        case (x >= 5 && x < 10):
            return [-34.33, -15.67];
            break;
        case (x >= 10 && x < 20):
            return [-15.67, 3];
            break;
        case (x >= 20 && x < 30):
            return [3, 21.67];
            break;
        case (x >= 30 && x < 45):
            return [21.67, 40.33];
            break;
        case (x >= 45 && x < 60):
            return [40.33, 59.00];
            break;
        case (x >= 60 && x < 80):
            return [59, 77.67];
            break;
        case (x >= 80 && x < 100):
            return [77.67, 96.33];
            break;
        case (x >= 100 && x < 125):
            return [96.33, 115];
            break;
        case (x >= 125 && x < 150):
            return [115, 133.67];
            break;
        case (x >= 150 && x < 180):
            return [133.67, 152.33];
            break;
        case (x >= 180 && x < 210):
            return [152.33, 171];
            break;
        case (x >= 210):
            return [168, 171];
            break;
    }
}
//Custom Speedometer Mbps to Angle conversion for 30Mbps case
function mbpsToAngle30 (x){
    switch (true) {
        case (x < 2.5):
            return [-53, -34.33];
            break;
        case (x >= 2.5 && x < 5):
            return [-34.33, -15.67];
            break;
        case (x >= 5 && x < 7.5):
            return [-15.67, 3];
            break;
        case (x >= 7.5 && x < 10):
             return [3, 21.67];
            break;
        case (x >= 10 && x < 12.5):
         	  return [21.67, 40.33];
            break;
        case (x >= 12.5 && x < 15):
            return [40.33, 59.00];
            break;
        case (x >= 15 && x < 17.5):
            return [59, 77.67];
            break;
        case (x >= 17.5 && x < 20):
            return [77.67, 96.33];
            break;
        case (x >= 20 && x < 22.5):
            return [96.33, 115];
            break;
        case (x >= 22.5 && x < 25):
            return [115, 133.67];
            break;
        case (x >= 25 && x < 27.5):
            return [133.67, 152.33];
            break;           
        case (x >= 27.5 && x < 30):
            return [152.33, 171];
            break;
        case (x >= 30):
            return [168, 171];
            break;
    }
}
function mbpsToAngle60 (x){
	return mbpsToAngle30(x/2);
}
function mbpsToAngle10 (x){
    switch (true) {
        case (x < .25):
            return [-53, -34.33];
            break;
        case (x >= .25 && x < .5):
            return [-34.33, -15.67];
            break;
        case (x >= .5 && x < 1.0):
            return [-15.67, 3];
            break;
        case (x >= 1.0 && x < 1.5):
            return [3, 21.67];
            break;
        case (x >= 1.5 && x < 2.25):
            return [21.67, 40.33];
            break;
        case (x >= 2.25 && x < 3.0):
            return [40.33, 59.00];
            break;
        case (x >= 3.0 && x < 4.0):
            return [59, 77.67];
            break;
        case (x >= 4.0 && x < 5.0):
            return [77.67, 96.33];
            break;
        case (x >= 5.0 && x < 6.25):
            return [96.33, 115];
            break;
        case (x >= 6.25 && x < 7.5):
            return [115, 133.67];
            break;
        case (x >= 7.5 && x < 9.0):
            return [133.67, 152.33];
            break;
        case (x >= 9.0 && x < 10.5):
            return [152.33, 171];
            break;
        case (x >= 10.5):
            return [168, 171];
            break;
    }
}
function mbpsToAngle6 (x){
	return mbpsToAngle10(x * 1.667);
}

function speedFactor (x,y){
    return x/y;
}
function roundHalf (n) {
    return (Math.round(n*2)/2).toFixed(2);
}
function scrollPage(){
    var currentY = $(window).scrollTop();
    $("html, body").animate({ scrollTop: currentY + 150 }, 600);
}
function isIE8 (){
    // Detecting IE8
    var oldIE;
    if ($('html').is('.ie8')) {
        speedTest.el.ie8 = true;
    }
}

// check to see when swf is fully loaded
//  http://learnswfobject.com/advanced-topics/executing-javascript-when-the-swf-has-finished-loading/
function swfLoadEvent(e, fn){
    //Ensure fn is a valid function
    if(typeof fn !== "function"){ return false; }
    
		//Set up a timer to periodically check value of PercentLoaded
		var loadCheckInterval = setInterval(function (){
				if(typeof e.ref.PercentLoaded !== "undefined" && e.ref.PercentLoaded()){
					//console.log('loaded:', e.ref.PercentLoaded());
					if(e.ref.PercentLoaded() == 100){
						//Execute function
						fn();
						//Clear timer
						clearInterval(loadCheckInterval);
					}								
				} else {
					//console.log('loaded: not yet');
				}
		}, 100); 
}
//This function is invoked by SWFObject once the <object> has been created
var swfCallback = function (e){
    if( e && (!e.success || !e.ref)){ return false; }
    swfLoadEvent(e, function(){
      //Put your code here
      speedTest.swfLoaded = true;
    });
 
};

