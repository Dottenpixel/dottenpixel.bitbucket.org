$(document).ready(function () {
    $('#insertLeadBtn').click(function () {
        $("#form-submitting").removeClass('CompleteDisp-No').addClass('CompleteDisp-Yes');
        $("#leadButtonContainer").removeClass('CompleteDisp-Yes').addClass('CompleteDisp-No');
        setTimeout(checkForErrors, 1000);
    });

    function checkForErrors() {
        if ($("#individualFieldError").length > 0) {
            $("#form-submitting").removeClass('CompleteDisp-Yes').addClass('CompleteDisp-No');
            $("#leadButtonContainer").removeClass('CompleteDisp-No').addClass('CompleteDisp-Yes');
        }
    }
});