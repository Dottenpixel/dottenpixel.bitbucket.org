$(document).ready(function() {
	
	/**
	 * Setup common AJAX values
	 */
	$.ajaxSetup({
	 dataType : 'json',
	 type : 'POST',
	 cache : false
	});
	
	bindFindServiceNearby();
});


function bindFindServiceNearby(){
	
	// content must have these ids : findServiceNearbyBtn, zipCodeServiceNearby
	$('#findServiceNearbyBtn').click(function(){
		 bindFindServiceNearbyRestCall();
	});
	
	// content must have these ids : findServiceNearbyBtn, zipCodeServiceNearby
	$('#findServiceNearbyForm').submit(function(){
		return bindFindServiceNearbyRestCall();
	});
	
	
	function bindFindServiceNearbyRestCall()
	{
		var zip = $('#zipCodeServiceNearby').val();
		var intRegex = /^\d+$/;
		if (isNullOrUndefined(zip)||!intRegex.test(zip) || zip.length < 5){
			 $('#textError-zipcode').text("Please enter a valid  zip code");
	        $('#textError-zipcode').attr('style', 'color: red');
			return false;
		}
		var inputData = {
		         "atg-rest-depth": "1",
		         "arg1" : zip
		};

		$.ajax({
			url : '/rest/bean/com/charter/serviceability/facade/ServiceabilityFacade/getServiceabilityRedirectPage',
			contentType: 'application/json',
			async       : false,        
			data        : JSON.stringify(inputData),
			success: function(data) {
				window.location = data.atgResponse;
			},
			error: function(data) {
				result = false;
			}
		});

		return false;
	}
}

