$(document).ready(function (){
	$("button#submitFormGreenButton").on("click",function(){
	    var utag_data_object = {};
        var datastore = $(this).data();
        if (datastore) {
            $.each(datastore,function(key,value) {
                utag_data_object[key] = value;
            });
            utag.link(utag_data_object); 
        }
		$("input#submitFormButton").click();
	});
	
});

/**
 * Success method.
 */
function oneStopCashFormSuccess(data) {
	var response = data.component.restResponse;
	
	// normal path
    if (response.success) {
    	
    	resetContactUsFormFields() ;
    	
    	
    	
        if (response.submittedBefore) {
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').text(
					"The form is already submited");
			$('#hereWeDisplayIfTheFormIsAlreadySubmitted').attr('style',
					'color: red');
		} else {
			$('#contactInfosResultDiv').text("Contact informations has been submited");
			$('#contactInfosResultDiv').attr('style', 'color: #38610B');
		}
    }

	// evaluate error
    else {
    	 
    	 var errorMessageHtml = "";
    	    for (var i=0;i<data.component.restResponse.errors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.errors[i].message + " " + data.component.restResponse.errors[i].errorCode + "<br/>";
    	    }
    	    for (var i=0;i<data.component.restResponse.fieldErrors.length;i++) {
    	        errorMessageHtml += data.component.restResponse.fieldErrors[i].message + " " + data.component.restResponse.fieldErrors[i].errorCode + "<br/>";
    	    }
    	   
    	    resetContactUsFormFields() ;
                
    	    if(errorMessageHtml.indexOf("businessName", 0)>0){
	            $('#businessName').attr('class','error-box');
	            $('#businessName-error-txt').html("Please enter the contact’s business name.");
	        }
	    
    	    if(errorMessageHtml.indexOf("firstName", 0)>0){
    	            $('#firstName').attr('class','error-box');
    	            $('#firstName-error-txt').html("Please enter the contact's first name.");
    	    }
    	    if(errorMessageHtml.indexOf("lastName", 0)>0){
	            $('#lastName').attr('class','error-box');
	            $('#lastName-error-txt').html("Please enter the contact's last name.");
	        }
	    
    	    if(errorMessageHtml.indexOf("zipCode", 0)>0){
	            $('#zipCode').attr('class','error-box');
	            $('#zipCode-error-txt').html("Please enter a valid zip code.");
	        }
	        if(errorMessageHtml.indexOf("emailAddress", 0)>0){
            $('#emailAddress').attr('class','error-box');
            $('#emailAddress-error-txt').html("Please enter a valid email address.");
            }
    }
}


function resetContactUsFormFields() {
	  $('#businessName').attr('class','businessName');
      $('#businessName-error-txt').html("");  	    
      
      $('#firstName').attr('class','firstName');
      $('#firstName-error-txt').html("");

      $('#lastName').attr('class','lastName');
      $('#lastName-error-txt').html("");

      $('#zipCode').attr('class','zipCode');
      $('#zipCode-error-txt').html("");

      $('#emailAddress').attr('class','emailAddress');
      $('#emailAddress-error-txt').html("");
      
     
      $('#contactInfosResultDiv').html("");
      $('#hereWeDisplayIfTheFormIsAlreadySubmitted').html("");
     
}