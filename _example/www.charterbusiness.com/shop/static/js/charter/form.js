var FORM = typeof FORM === 'undefined' ? {} : FORM;

FORM.showValidationErrors = function(validationErrors, messageMap) {
	if(validationErrors != null){
		for (var i=0; i < validationErrors.length; i++) {
			var validationError = validationErrors[i];
			var fieldId = validationError.fieldKey;
			var errorKey = validationError.errorKey;
			var actionName = errorKey.substr(errorKey.indexOf(".") + 1);
			showInputFieldError(fieldId, messageMap[actionName]);
		}
	}
};

FORM.showRequiredErrors = function(fieldsInError, messageMap) {
	for (var i=0; i < fieldsInError.length; i++) {
		var fieldId = fieldsInError[i];
		showInputFieldError(fieldId, messageMap['required']);
	}
};

FORM.showFormResultErrors = function(resultError) {
	if(resultError != null){
		if(resultError.code == 'INVALID_ADDRESS'){
			$("#errorDiv").html('<span class="error-txt">Please verify the Address,Zip and re-submit the form.</span>');
			$("#force_scrub_address_id").val('false');
		}else {
			$("#errorDiv").html('<span class="error-txt">Error ! Please try later.</span>');
		}
	}
};

FORM.clearErrors = function(errorDiv) {
	clearPreviousErrors(errorDiv);
};

FORM.checkRequiredFields = function(formId) {
	var $form = $("#" + formId);
	var $requiredFields = $form.find('input.required, select.required, textarea.required');
	var noDataFields = [];

	// remove placeHolder text if field is empty
	updatePlaceHolderForm($form, true);
	
	$requiredFields.each(function() {
		var type = this.type;
		var fieldId = this.id;
		if (type == 'text' || type == 'password' || type == 'select' || type == 'number' || type == 'select-one') {
			var value = $(this).val().length == 0 ? '' : $.trim($(this).val());
			if (value.length == 0) {
				noDataFields.push(fieldId);
			}
		} else if (type == 'checkbox') {
			if (!$(this).is(':checked')) {
				noDataFields.push(fieldId);
			}
		} else if (type == 'radio') {
			var filter = $requiredFields.filter('[name=' + name + ']:checked');
			if (filter.length == 0) {
				noDataFields.push(fieldId);
			}
		}
	});
	
	// put back placeHolder text if field is empty
	updatePlaceHolderForm($form, false);
	
	return noDataFields;
};