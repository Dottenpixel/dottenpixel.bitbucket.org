
// @see com.charter.commerce.checkout.enums.ApplicableToType.productType
var PRODUCT = "Product";
var INTERNET = "internet" + PRODUCT;
var HOMEPHONE = "homePhone" + PRODUCT;
var TELEVISION = "tv" + PRODUCT;
var PRODUCT_TYPES = [INTERNET, HOMEPHONE, TELEVISION];

//buy flow type @see com.charter.storefront.type.BuyflowEntryPointType
var ANALYSER = "CB_ANALYSER";
var BUNDLE_BUILDER = "CB_BUNDLE_BUILDER";
var FEATURE_OFFER = "CB_FEATURE_OFFER";

/**
 * START
 * this switch is to use the jsp for the charter business checkout pages instead of the one that are in content instead
*/
//default is false, switch to true to go back to old jsps
var useOldJspForCheckoutFlow = false;
var storefront_url = "/storefront";
var bundles_url = "/bundles-old";
var customize_page_url = "/checkout/customize";
var phone_porting_page_url = "/checkout/phone-porting";
var service_agreements_page_url = "/checkout/service-agreements";
var contact_info_page_url = "/checkout/contact-info";
var confirmation_page_url = "/checkout/confirmation";
var schedule_isntallation_page_url = "/checkout/schedule-installation";
var service_exception_page_url =  "/content/service-error";
var confirmation_enrollment_url =  "/content/tools-enrollment-confirmation";
var confirmation_referral_url =  "/content/tools-referral-confirmation";
var expiration_referral_url =  '/?expiration=session';
var refer_business_url =  '/content/refer-business';
var tools_submit_info_url =  '/content/tools-submit-info';
var tools_agent_submit_info_url =  '/content/tools-agent-submit-info';
var tools_thank_you_url =  '/content/tools-thank-you';
var thank_you_contact_us =  '/content/thank-you-contact-us';
var optout_direct_mail = '/content/optout-direct-mail';
var optout_direct_mail_thank_you = '/content/thank-you-optout-direct-mail';
var not_serviceable_url =  '/content/not-serviceable';
var lead_gen_form_url = '/lead-gen-form';
var esignStepTwo = '/content/business-agent-esign?e=2';



if(useOldJspForCheckoutFlow) {
	customize_page_url = "/shop/bundles/customize.jsp";
	phone_porting_page_url = "/shop/buyflows/phone_porting.jsp";
	service_agreements_page_url = "/shop/buyflows/service_agreements.jsp";
	contact_info_page_url = "/shop/buyflows/contact_info.jsp";
	confirmation_page_url = "/shop/buyflows/confirmation.jsp";
	schedule_isntallation_page_url = "/shop/buyflows/schedule_installation.jsp";
	service_exception_page_url =  "/shop/tools/service_request_error.jsp";
    confirmation_enrollment_url =  "/shop/tools/enrollment_request_confirmation.jsp";
    confirmation_referral_url =  "/shop/tools/business_confirmation_referral.jsp";
    refer_business_url =  "/shop/tools/refer_business.jsp";
    expiration_referral_url =  '/?expiration=session';
	$('a#lnkEstimate').attr('href',"/shop/bundles/customize.jsp");
}
/**
 * END checkout page switch
 */

var clearCartUrl = bundles_url;

/**
 * Setup common AJAX values
 */
function ajaxSetup() {
	$.ajaxSetup({
		dataType : 'json',
		type : 'POST',
		cache : false,
		error: ajaxErrorDefault
	});
}

/**
 * On document ready do something
 */
$(document).ready(function() {

	// generic stuff
	ajaxSetup();
	bindAllRestForm();
	activatePlaceHolder();

	// ATG stuff
	updateDYNSessConf();

	// Charter stuff
	retrieveCurrentOrderId();
	bindClearCart();
	validateWithEnterKeyAndSubmitGoogleSearch();

	/**
	 * POPUP OVERLAYS
	 */
	bindLoxWithModal();

});






/**
 * Utility method to bind lox with modal.
 */
function bindLoxWithModal(){
	$modalDivs = $('.modal:not(.no-lox)');
	if ($modalDivs.length > 0 && $modalDivs.lox != undefined) {
		$modalDivs.lox();
	}
}

/**
 * Default method to call when ajax error occurs.
 * @param data
 */
function ajaxErrorDefault(request, status, error) {
	if (request.status == 0) {
		return;
	}

	if (status == "parsererror") {
		window.console && console.log("Error while parsing data: " + request.statusText + " (" + request.status + ", " + error + "\n" + request.responseText + ")");
		return;
	}

	// default behavior is to redirect to session expired page
	window.console && console.log("AJAX error when calling " + this.url + ": " + request.statusText + " (" + request.status + ", " + error + ") then redirecting to " + expiration_referral_url);
	//window.location = expiration_referral_url;
}

/**
 * Bind all the form who have class=rest-form.
 */
function bindAllRestForm() {
	$(".rest-form").each(function() {
		var form = $(this);
		var async = !form.hasClass("sync");
	    bindForm(form, form.attr("data-success"), form.attr("data-error"), async)
	});
}

/**
 * Bind the form submit button to AJAX.
 *
 * @param form the form
 * @param successFunction the success function
 * @param errorFunction the error function
 * @return false when submitted by AJAX
 */
function bindForm(form, successFunction, errorFunction, async) {

	// check if successFunction is a function
	if (!$.isFunction(successFunction)) {
		successFunction = window[successFunction];
	}

	// check if errorFunction is a function
	if (!$.isFunction(errorFunction)) {
		errorFunction = window[errorFunction];
	}

	// no error function then use the default error function
	if (!$.isFunction(errorFunction)) {
		errorFunction = ajaxErrorDefault;
	}

	if (async == undefined) {
		async = true;
	}

    form.unbind();
    form.bind("submit", function() {
        $.ajax({
            url: form.attr('action'),
            data: formSerialize(form),
        	async : async,
            success: successFunction,
            error: errorFunction
        });
        return false;
    });
}

/**
 * automatically Bind the form to AJAX.
 *
 * @param form the form
 * @param successFunction the success function
 * @param errorFunction the error function
 * @return result from AJAX call
 */
function autoBindForm(form, successFunction, errorFunction) {

	// check if successFunction is a function
	if (!$.isFunction(successFunction)) {
		successFunction = window[successFunction];
	}

	// check if errorFunction is a function
	if (!$.isFunction(errorFunction)) {
		errorFunction = window[errorFunction];
	}

	// no error function then use the default error function
	if (!$.isFunction(errorFunction)) {
		errorFunction = ajaxErrorDefault;
	}

	var result;
	$.ajax({
        url: form.attr('action'),
        data: formSerialize(form),
    	async : false,
        success: function(data) {
            result = data;
        },
        error: errorFunction
    });

	return result;
}

//**********************************************************************************************
// ATG specific functions
//**********************************************************************************************

/**
 * Update all session confirmation number.
 */
var dynSessConf="";
function updateDYNSessConf() {
	if(dynSessConf==""){
	    $.ajax({
	        url: '/rest/bean/com/charter/rest/RestSession/retrieveSessionConfirmationNumber',
	        async : false,
	        success: function(data) {
	            dynSessConf=data.atgResponse
	            populateDynSessConf();
	        }
	    });
	}else{
		populateDynSessConf();
	}
}

var populateDynSessConf = function() {
    $('input[name=_dynSessConf]').val(dynSessConf);
}

//**********************************************************************************************
// CHARTER specific functions
//**********************************************************************************************

/**
 * Retrieve current order id and set to input with name pageOrderId.
 */
function retrieveCurrentOrderId() {
	var result = false;
	$.ajax({
        url : '/rest/bean/com/charter/rest/RestSession/retrieveCurrentOrderId',
        async : false,
        success: function(data) {
            $('input[name=pageOrderId]').val(data.atgResponse);
            result = true;
        }
    });
    return result;
}

/**
 * Reset cart and start over.
 *
 * @param redirectFnct function to call on ajax success
 * @returns true on ajax success
 */
function clearCart(redirectFnct) {
	var result = false;
    $.ajax({
        url : '/rest/bean/com/charter/commerce/services/checkout/ClearCheckoutFacade/resetCheckout',
        async : false,
        success : function(data) {
        	if ($.isFunction(redirectFnct)) {
        		redirectFnct();
        	}
        	result = true;
        }
    });
    return result;
}

/**
 * Bind the clear cart button.
 */
function bindClearCart() {
	var referer = getURLParameter("referer");
	if (referer != null) {
		clearCartUrl = referer;
	}

	$("[id$='restart-builder']").bind("click", function() {
		clearCart(function() {
			window.location = clearCartUrl;
		});
		return false;
	});
}

/**
 * Retrieve ServiceAbilityTO from session.
 *
 * @return the ServiceAbilityTO
 */
var serviceAbilityTO = undefined;
function retrieveServiceAbilityTO() {
	if (serviceAbilityTO != undefined) {
		return serviceAbilityTO;
	}

	var inputData = {
		"atg-rest-depth": "4"
	};

	$.ajax({
		url 		: '/rest/bean/com/charter/commerce/services/checkout/CharterSessionFacade/retrieveServiceAbilityTO',
		contentType : 'application/json',
		async       : false,
		data        : JSON.stringify(inputData),
		success     : function(data) {
			serviceAbilityTO = data;
		}
	});
	return serviceAbilityTO;
}

/**
 * Reset bundling session
 */
function resetBuyflowEntryPointType() {
	var inputData = {
		"atg-rest-depth": "1"
	};

	var result = false;
	$.ajax({
		url 		: '/rest/bean/com/charter/commerce/services/checkout/CharterSessionFacade/resetBuyflowEntryPointType',
		contentType : 'application/json',
		async       : false,
		data        : JSON.stringify(inputData),
		success     : function(data) {
			result = true;
		}
	});
	return result;
}
/**
 * Method to retrieve ServiceAbilityTO and build the address section.
 */
function displayServiceAbilityAddress() {
	var serviceAbilityTO = retrieveServiceAbilityTO();
	if(serviceAbilityTO.scrubbedAddressTO != undefined) {
		serviceAddress = serviceAbilityTO.scrubbedAddressTO;
		if (serviceAddress.address1 != null &&
				serviceAddress.city != null &&
				serviceAddress.state != null &&
				serviceAddress.postalCode != null) {
			$("div#customer-address").html(serviceAddress.address1+"<br/>"+serviceAddress.city +", "+serviceAddress.state + "  "+serviceAddress.postalCode);
		}
	}
}

/**
 * Retrieve CustomerContactTO from session.
 *
 * @return the CustomerContactTO
 */
var customerContactInfo = undefined;
function retrieveCustomerContactInfo() {
	if (customerContactInfo != undefined) {
		return customerContactInfo;
	}

	var inputData = {
		"atg-rest-depth": "4"
	};

	$.ajax({
		url 		: '/rest/bean/com/charter/commerce/services/checkout/CharterSessionFacade/retrieveCustomerContactInfo',
		contentType : 'application/json',
		async       : false,
		data        : JSON.stringify(inputData),
		success     : function(data) {
			customerContactInfo = data;
		}
	});
	return customerContactInfo;
}

//**********************************************************************************************
// Utility functions
//**********************************************************************************************

/**
 * Replace attribute for elem specific element.
 *
 * @param elem
 * @param attrName
 * @param searchValue
 * @param newValue
 */
function replaceAttr(elem, attrName, searchValue, newValue) {
	var attr = elem.attr(attrName);
	if (attr != undefined) {
	    var newAttrValue = attr.replace(searchValue, newValue);
	    elem.attr(attrName, newAttrValue);
	}
}

/**
 * Replace attribute for all child elem specific element.
 *
 * @param elem
 * @param attrNames
 * @param jsonObj {searchValue: newValue, ... }
 */
function replaceAttrForChild(elem, attrNames, jsonObj) {
	if (attrNames.length <= 0) {
		return;
	}

	var selectorString = "";
	for (var x = 0; x < attrNames.length; x++) {
		if (x > 0) {
			selectorString += ", ";
		}
		selectorString += "*["+attrNames[x]+"]";
	}

    elem.find(selectorString).each(function() {
    	var matchElem = $(this);
        $.each(jsonObj, function(searchValue, newValue) {
        	for (var x = 0; x < attrNames.length; x++) {
        		replaceAttr(matchElem, attrNames[x], searchValue, newValue);
        	}
        });
    });
}

/**
 * Utility function to check null or undefined (just to avoid repeated lines).
 *
 * @param source input to check.
 * @returns true if provide source is null or undefined, else false.
 */
function isNullOrUndefined(source){
	return (source == null || typeof source == 'undefined' ||source == '' ) ? true : false;
}

/**
 * Utility function to lower case the first char of a string.
 *
 * @param str the string
 * @returns the new string
 */
function lowerCaseFirstChar(str) {
	if (str.length == 0) {
		return str;
	}
	return str[0].toLowerCase() + str.slice(1)
}

/**
 * Utility function to upper case the first char of a string.
 *
 * @param str the string
 * @returns the new string
 */
function upperCaseFirstChar(str) {
	if (str.length == 0) {
		return str;
	}
	return str[0].toUpperCase() + str.slice(1)
}

/**
 * Create a new threads.
 *
 * @param fnct the function to call
 * @param arguments the argument list
 * @returns the function result
 */
function thread(fnct) {
	var args = [fnct, 0];
	args = args.concat(Array.prototype.slice.call(arguments, 1));
	threadWait.apply(null, args);
};

/**
 * Wait and create a new threads.
 *
 * @param fnct the function to call
 * @param arguments the argument list
 * @returns the function result
 */
function threadWait(fnct, wait) {
    var args = Array.prototype.slice.call(arguments, 2);
    return setTimeout(function() {
    	return fnct.apply(null, args);
	}, wait);
};

/**
 * Get URL param.
 * @see http://stackoverflow.com/questions/1403888/get-url-parameter-with-jquery
 *
 * @param name the param
 * @returns the value
 */
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

/**
 * Function to check if place holder is supported by the browser
 * @returns {Boolean} true if suppported
 */
var placeHolderSupported = undefined;
function isPlaceHolderSupported() {
	if (placeHolderSupported == undefined) {
		placeHolderSupported = 'placeholder' in document.createElement('input');
	}
	return placeHolderSupported;
}

/**
 * Activate place holder for browser who don't support it
 */
function activatePlaceHolder() {
	if (isPlaceHolderSupported()) {
		return;
	}

	$('[placeholder]').focus(function() {
		updatePlaceHolderInput($(this), true);
	}).blur(function() {
		updatePlaceHolderInput($(this), false);
	}).blur();
}

/**
 * Update input value depending on the place older.
 *
 * @param input the input
 * @param remove flag to indicate to remove or put back placeHolder text
 */
function updatePlaceHolderInput(input, remove) {
	if (isPlaceHolderSupported()) {
		return;
	}

	if (remove) {
		if (input.val() == input.attr('placeholder')) {
			input.val('');
		}
	} else {
		if (input.val() == '') {
			input.val(input.attr('placeholder'));
		}
	}
}

/**
 * Update the placeHolder text for all input field of the form.
 *
 * @param form the form
 * @param remove flag to indicate to remove or put back placeHolder text
 */
function updatePlaceHolderForm(form, remove) {
	if (isPlaceHolderSupported()) {
		return;
	}

	form.find('[placeholder]').each(function() {
		updatePlaceHolderInput($(this), remove);
	});
}

/**
 * Return the place holder data serialized.
 *
 * @param form the form to get the data
 */
function formSerialize(form) {
	if (isPlaceHolderSupported()) {
		return form.serialize();
	}

	updatePlaceHolderForm(form, true);
	var data = form.serialize();
	updatePlaceHolderForm(form, false);
	return data;
}

/**
 * Return the place holder data serialized array.
 *
 * @param form the form to get the data
 */
function formSerializeArray(form) {
	if (isPlaceHolderSupported()) {
		return form.serializeArray();
	}

	updatePlaceHolderForm(form, true);
	var data = form.serializeArray();
	updatePlaceHolderForm(form, false);
	return data;
}
//**********************************************************************************************
//Format and validations functions
//**********************************************************************************************

/**
 * Format the phone input after change event.This can be plugged to any text input with CSS class "phoneformat"
 */
function bindPhoneForFormatDisplay(){
	$('input.phoneformat').bind('keyup change paste', function(e) {
		var $phone = $(this).val();
		if(!isNullOrUndefined($phone) && e.keyCode != 8 && e.keyCode != 46){
			phoneChangeEvent($phone, $(this));
		}
	});
}

/**
 * utility method to add hyphen symbol in phone number.
 */
function phoneChangeEvent($phone,item){
	$phone = formatPhoneNumberWithoutHyphen($phone);
	if($phone.length == 3){
		$(item).val($phone+'-');
	}else if($phone.length > 3 && $phone.length <6){
		$(item).val($phone.substr(0, 3)+'-'+$phone.substr(3));
	}else if($phone.length == 6){
		$(item).val($phone.substr(0, 3)+'-'+$phone.substr(3,3)+'-');
	}else if($phone.length >= 7 && $phone.length <=10){
		$(item).val($phone.substr(0, 3)+'-'+$phone.substr(3,3)+'-'+$phone.substr(6));
	}else if($phone.length >=10){
		$(item).val($phone.substr(0, 3)+'-'+$phone.substr(3,3)+'-'+$phone.substr(6,4));
	}
}

/**
 * Add hyphen symbols on phone input for display purpose.
 */
function formatPhoneNumber(input) {
	if(isNullOrUndefined(input)){
		return '';
	}
    var numbers = input.replace(/\D/g, '');
    var count = 0;
    return "XXX-XXX-XXXX".replace(/X/g, function() {
        return numbers.charAt(count++);
    });
}

/**
 * Remove hyphen symbols to make them valid phone numbers. this is useful while submitting the values before submit.
 */
function formatPhoneNumberWithoutHyphen(input) {
	if(isNullOrUndefined(input)){
		return '';
	}
    var phoneNumber = input.replace(/-/g, '');
    return phoneNumber;
}

/**
 * Format the input phone number as per the proposed format.
 */
function formatPhoneNumberWithFormat(input,format) {
	if(isNullOrUndefined(input) || isNullOrUndefined(format)){
		return '';
	}
    var numbers = input.replace(/\D/g, '');
    var count = 0;
    return format.replace(/X/g, function() {
        return numbers.charAt(count++);
    });
}

/**
 * Utility validation for google search box.
 */
function validateAndSubmitGoogleSearch(){
	var queryString = $.trim($('input#search-field').val());
	var placeholderStr = $('input#search-field').attr('placeholder');
    // We do not submit if queryString is on the below.
	// a) empty.
	// b) undefined.
	// c) its equals to placeholder string.
	if(isNullOrUndefined(queryString) || placeholderStr == queryString){
		return false;
	}else{
		$('#googleSearchSubmit').click();
	}
}

/**
 * Utility validation for google search box  when  click  enter key.
 */
function validateWithEnterKeyAndSubmitGoogleSearch(){
	$("input#search-field").keydown(function(event) {
	if (event.keyCode == 13) {
    	var queryString = $.trim(this.value);
    	var placeholderStr = $(this).attr('placeholder');
    	if(isNullOrUndefined(queryString) || placeholderStr == queryString){
    		return false;
    	}else{
    		$('#googleSearchSubmit').click();
    	}
    }
	});
}

/**
 *  Formatting the phone number
 **/

function formatPhone(obj) {
	var numbers = obj.value.replace(/\D/g, ''),
	char = {0:'',3:'-',6:'-'};
	obj.value = '';
	for (var i = 0; i < numbers.length; i++) {
	  if(i>9){obj.value += (char[i]||'');}
	  else{obj.value += (char[i]||'') + numbers[i];}
	}
}

/**
 *  Formatting the zip code
 **/

function formatZipCode(obj) {
	var numbers = obj.value.replace(/\D/g, ''),
	char = {5:''};
	obj.value = '';
	for (var i = 0; i < numbers.length; i++) {
	  if(i>4){obj.value += (char[i]||'');}
	  else{obj.value += (char[i]||'') + numbers[i];}
	}
}

function goToBundleBuilder(){
	window.location = bundles_url;
}

function goToLeadGenForm(){
	window.location= lead_gen_form_url;
}

/* Add files to be imported */
var jsBFiles = new Array();
var shopPrefix='/shop';
jsBFiles.push(shopPrefix+'/static/js/charter/user_experience.js');

function addJSNode(url) {
	document.write("<script type='text/javascript' src='" + url + "'></script>");
}
for (var i = 0; i < jsBFiles.length; i++) {
	addJSNode(jsBFiles[i]);
}


