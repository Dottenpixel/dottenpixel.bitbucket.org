/**
 * Highlight error fields. This utility method works on validation exceptions only (i.e fieldErrors from restResponse only).
 * 
 * @param data the data
 */
function highlightErrorFields(data) {
	//check for validation errors in RestResponse
	if(data.component.restResponse.fieldErrors == null) {
		return;
	}
		
	// find all required fields from the current form.
	var $errors = $('.required,.required2'); 
	var $fieldErrors = data.component.restResponse.fieldErrors;
	
	for(i = 0; i < $fieldErrors.length; i++) {
		
		// process and retrieve the error field.
		var errorFieldId = $fieldErrors[i].errorCode;
		
		//check if the required field is in the errors list or not.
		if(findFieldErrorFromArray(errorFieldId,$errors) != undefined) {
			//Note: May need to change style to make it flexible with other input types( radio, checkbox ...etc)
			$(formatIdsWithDot(errorFieldId)+'_label').attr('style','color: #DF0101');
		}
	}
}

/**
 * Check if the required field is in the errors list or not.
 * 
 * @param errorFieldId the error field id.
 * @param errors list of required inputs in the form.
 * @return true if error field id is found for required field.
 */
function findFieldErrorFromArray(errorFieldId, errors) {
    return $.grep(errors, function (n, i) {
        return(n.id == errorFieldId);
    })[0];
}

/**
 * Retrieve the error field. This will process the string and returns substring before the last occurrence of "." charcter.
 * Examples:-
 * 1) Source: aaa.bbb.ccc , Result: aaa.bbb
 * 2) Source: aaa.bbb.c , Result: aaa.bbb
 * 3) Source: aaa.bbb , Result: aaa
 * 4) Source: aaa , Result: aaa
 * 5) Source: aaabbbccc , Result: aaabbbccc
 * 
 * @param id the id
 */
function getErrorFieldId(errorId){
	return errorId.substring(0,errorId.lastIndexOf('.'));
}

/**
 * Work around utility method to replace all . with \\. so that we can get find element by id. 
 * For example, an element with id="foo.bar", can use the selector $("#foo\\.bar").
 * 
 * @param id the id
 */
function formatIdsWithDot(id) {
    return "#" + id.replace( /(\.)/g, "\\$1" );
}

/**
 * Reset the style of required fields.
 */
function resetRequiredFields() {
	$('.required,.required2').attr('style','');
}

/**
 * Processing the form errors , not related to any input fields.
 * These are errors that are returned  in the "errors" json element.
 *
 * @param errors the errors
 * @param errorDivId the div id
 */
function processFormErrors(errors, errorDivId){
	if(errors.length <= 0) {
		return;
	}

	var ulElement = $("<ul id='formErrors' style='list-style-type:none; color:red'> </ul>")
	$.each(errors, function(){
		var errorMessage = this.message ; 
		var liElement = $("<li/>")
		liElement.append(errorMessage)
		ulElement.append(liElement)
	});
	
	$("#"+errorDivId).append(ulElement);
}

/**
 * Processing the form errors, not related to any input fields. 
 * These are errors that are returned  in the "fieldErrors" json element.
 * if we find an input field that has the same id as the error code, this error is displayed right under the input field
 * 
 * @param fieldErrors the json
 * @param errorDivId the div id
 */
function processFieldErrors(fieldErrors, errorDivId){
	if(fieldErrors.length <= 0) {
		return;
	}
		
	var ulElement = $("<ul id='fieldErrors' style='list-style-type:none; color:red'> </ul>")
	$.each(fieldErrors, function(){
		
		var fieldId = $.trim(this.errorCode);
		var errorMessage = this.message ;
		
		if((fieldId != "")&& ($("input#"+fieldId).length > 0)){
			showInputFieldError(fieldId, errorMessage);
		} else {	
			var liElement = $("<li/>");
			liElement.append(errorMessage);
			ulElement.append(liElement);
		}
	});
	
	$("#"+errorDivId).append(ulElement);
}

/**
 * Shows the error message for the given field.
 * 
 * @see processFieldErrors
 */
function showInputFieldError(fieldId, errorMessage) {
	var divFieldError = $("<div id='individualFieldError' style='margin-bottom:25px; color:red'>"); 
	divFieldError.append(errorMessage) ;
	$("input#"+fieldId+", select#"+fieldId).addClass("error-box");
	$("input#"+fieldId+", select#"+fieldId).parent().append(divFieldError);
}

/**
 * Clear the error added by processFieldErrors.
 * 
 * @see processFieldErrors
 * @param errorDiv
 */
function clearPreviousErrors(errorDiv){
	$("div#"+errorDiv).empty();
	$("div#individualFieldError").remove();
	$("input.error-box").removeClass("error-box");
}