// JavaScript Document
var cb_linkname = "";
var cb_pagename = "" + window.location.pathname;

(function ($) {

    $.fn.tealium_trackable = function (options) {
        var settings = $.extend({
            "section_1": "com",
                "section_2": window.location.pathname.split("/")[2],
                "section_3": "",
                "page_name": "com:" + window.location.pathname.split("/")[2]
        }, options);

        return this.each(function () {
            var utag_data_object = {};
            var datastore = $(this).data();
            var udo = {};
            var temp = {};
            $.each(datastore, function (key, value) {
                utag_data_object[key] = value;
            });
            temp = $.extend({}, utag_data_object, settings);
            udo = $.extend({}, window.utag_data, temp);
            if (!udo.page_type) {
                udo.page_type = "charter business";
            }
            $(this).click(function () {
                utag.link(udo);
            });
            $(this).focus(function () {
                utag.link(udo);
            });
            $(this).change(function () {
                utag.link(udo);
            });
        });
    }

}(jQuery));


$(function () {

    $(".tracked").tealium_trackable();
    $("#zipCodeServiceNearby").on("focus", function () {
        $(this).attr("placeholder", "");
    });

    $("#zipCodeServiceNearby").tealium_trackable({
        "event_cat": "localize",
            "event_action": "go localize cursor in zip box",
            "event_label": cb_pagename
    });
    $("#findServiceNearbyBtn").tealium_trackable({
        "event_cat": "localize",
            "event_action": "go localize submit zip",
            "event_label": cb_pagename
    });
    $(".hero_btn a").tealium_trackable({
        "event_cat": "hero and slots",
            "event_action": "hero click",
            "event_label": cb_pagename
    });
    $("#slot1_btn").tealium_trackable({
        "event_cat": "hero and slots",
            "event_action": "slot1 click",
            "event_label": cb_pagename
    });
    $("#slot2_btn").tealium_trackable({
        "event_cat": "hero and slots",
            "event_action": "slot2 click",
            "event_label": cb_pagename
    });
    $("#slot3_btn").tealium_trackable({
        "event_cat": "hero and slots",
            "event_action": "slot3 click",
            "event_label": cb_pagename
    });
    $("#livechat1").tealium_trackable({
        "event_cat": "chat",
            "event_action": "chat online now click",
            "event_label": cb_pagename
    });
    $("#livechat2").tealium_trackable({
        "event_cat": "chat",
            "event_action": "chat online now click alt",
            "event_label": cb_pagename
    });
    $("#livecall1").tealium_trackable({
        "event_cat": "click to call",
            "event_action": "click to call alt",
            "event_label": cb_pagename
    });

    $(".redir").find("option:selected").tealium_trackable();

    $("div[id^='livechat'] > a").tealium_trackable({
        "event_cat": "crm",
            "event_action": "click chat",
            "event_label": cb_pagename
    });
    $("div[id^='livecall'] > a").tealium_trackable({
        "event_cat": "crm",
            "event_action": "click call",
            "event_label": cb_pagename
    });
});
$(window).load(function() {

    //New Responsive Header & Footer Tracking

    // Responsive Header Tracking //

    //Top Bar Links
    $('.cb-navbar-top a').on("click", function () {
        cb_linkname = $(this).text();
        $(".cb-navbar-top a").tealium_trackable({
            "event_cat": "subnav",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });


    //Left Side Links

    $('.pull-right a').on("click", function () {
        cb_linkname = $(this).text();
        $(".pull-right a").tealium_trackable({
            "event_cat": "subnav",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });


    //Sub Nav Links

    $('.cb-subnav-container a').on("click", function () {
        cb_linkname = $(this).text();
        $(".cb-subnav-container a").tealium_trackable({
            "event_cat": "subnav",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });



    // Responsive Footer Tracking //

    //Main Links

    $('.footer-list a').on("click", function () {
        cb_linkname = $(this).text();
        $(".footer-list a").tealium_trackable({
            "event_cat": "footer",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });


    //Social Links

    $('.footer-social-icons a').on("click", function () {
        cb_linkname = $(this).text();
        $(".footer-social-icons a").tealium_trackable({
            "event_cat": "subnav",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });


    //Legal Links

    $('.footer-legal a').on("click", function () {
        cb_linkname = $(this).text();
        $(".footer-legal a").tealium_trackable({
            "event_cat": "subnav",
                "event_action": cb_linkname,
                "event_label": cb_pagename
        });
        //alert(cb_linkname);
        //console.log(cb_linkname);
    });
});