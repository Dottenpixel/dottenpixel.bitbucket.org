// enable or disable persistent cookies to function. True = run, False = disable
var enableCookies = true;

function runCBCookies() {

	if (enableCookies) {
		
		var url_vars = $.deparam.querystring();
		
		if (!/phone/i.test(url_vars.pageID)) {
		
			var cb_expdays = '30';
			var cb_paidtactic = ['LP_EM', 'LP_em_', 'LP_DM', 'LP_FSI', 'LP_TE', 'Evolve', 'evolve', 'EVOLVE', 'LP_AIMedia', 'LP_AiMedia', 'LP_DGS', 'LP_Leapfrog', 'LP_newbiz', 'LP_Saveology', 'LP_Simple', 'LP_SNS', 'TruEffect', 'Trueffect', 'LP_hosting', 'LP_PGConsulting', 'LP_more', 'LP_55'];
			
			var customPages = ['analyzer', 'contact-info'];
			var pageURL = window.location.href;
			
			var d = new Date();
			d.setTime(d.getTime() + (cb_expdays*24*60*60*1000));
			var cb_expires = 'expires='+d.toGMTString();
					
			if ($.isEmptyObject(url_vars.leadRef)) {
				// do nothing
				
				// check if TruEffect cookie exists, set CB AFFPN cookie to same value
				if (USEREXPERIENCE.getCookie('TE_PN')) {
					USEREXPERIENCE.setCookie('cb_affpn', USEREXPERIENCE.getCookie('TE_PN'), cb_expires);
				}
	
			} else {
				$.each(cb_paidtactic, function (i, val) {
					if (url_vars.leadRef) {
						if (url_vars.leadRef.indexOf(cb_paidtactic[i]) >= 0) {
							if (url_vars.pageID) {
								USEREXPERIENCE.setCookie('cb_affpn', url_vars.pageID, cb_expires);
								document.cookie = 'TE_PN=; path=/; domain=.charterbusiness.com; expires=' + new Date(0).toUTCString();
							}
							if (USEREXPERIENCE.getCookie('cb_first_touch')) {
								USEREXPERIENCE.setCookie('cb_last_touch', url_vars.leadRef, cb_expires);
								//console.log('Cookie last touch set = ' +url_vars.leadRef );
							} else {
								USEREXPERIENCE.setCookie('cb_first_touch', url_vars.leadRef, cb_expires);
								//console.log('Cookie first touch set = ' +url_vars.leadRef );
								USEREXPERIENCE.setCookie('cb_last_touch', url_vars.leadRef, cb_expires);
								//console.log('Cookie last touch set = ' +url_vars.leadRef );
							}
						}
					}
				});
			}
			
			// Deletes UserExp Cookie 
			function clearCBUserExpCookie() {
				USEREXPERIENCE.clearCookie('cb_affpn');
				USEREXPERIENCE.clearCookie('cb_first_touch');
				USEREXPERIENCE.clearCookie('cb_last_touch');
				document.cookie = 'TE_PN=; path=/; domain=.charterbusiness.com; expires=' + new Date(0).toUTCString();
			}
			
			$(document).ready(function(e) {
				

				//Lead Source Code - added in 10/23/2014
				if (url_vars.leadSource == '1') {
					$('#LeadSource').val('Web Lead - No Special Offer');
				} else if ( url_vars.leadSource != undefined && url_vars.leadSource != "") {
					$('#LeadSource').val(url_vars.leadSource);
				}		
				
				if ($.isEmptyObject(url_vars.leadRef)) {
					var isCustomPage = false;
					$.each(customPages, function (i, val) {
						if (pageURL.indexOf(customPages[i]) >= 0) {
							isCustomPage = true;
						}
					});
					if (USEREXPERIENCE.getCookie('cb_last_touch')) {
						if (isCustomPage) {
							$('input[name="leadReferredByUrl"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
							$('input[name="LeadReferredByUrl__c"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
						} else {
							$('input[name="LeadSubmittedBy"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
							$('input[name="LeadSubmittedBy__c"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
						}
					}
				} else {
					var isPaidTactic = false;
					$.each(cb_paidtactic, function (i, val) {
						if (url_vars.leadRef) {
							if (url_vars.leadRef.indexOf(cb_paidtactic[i]) >= 0) {
								isPaidTactic = true;
							}
						}
					});
					if (isPaidTactic) {
						if (USEREXPERIENCE.getCookie('cb_first_touch')) {
							//console.log('cookie first touch exists');
							$('input[name="LeadSubmittedBy__c"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
							$('input[name="LeadReferredByUrl__c"]').val(USEREXPERIENCE.getCookie('cb_first_touch'));
						} else {
							//console.log('cookie first touch does NOT exist');
							$('input[name="LeadSubmittedBy__c"]').val(USEREXPERIENCE.getCookie('cb_first_touch'));
							$('input[name="LeadReferredByUrl__c"]').val(USEREXPERIENCE.getCookie('cb_first_touch'));
						}
					} else {
						if (USEREXPERIENCE.getCookie('cb_last_touch')) {
							//console.log('cookie last touch exists');
							$('input[name="LeadSubmittedBy__c"]').val(USEREXPERIENCE.getCookie('cb_last_touch'));
							$('input[name="LeadReferredByUrl__c"]').val(url_vars.leadRef);
						} else {
							//console.log('cookie last touch does NOT exist');
							$('input[name="LeadSubmittedBy__c"]').val(url_vars.leadRef);
							$('input[name="LeadReferredByUrl__c"]').val(USEREXPERIENCE.getCookie('cb_first_touch'));
						}
					}
				}	
				
			});
		} else {
			if (/phone/i.test(USEREXPERIENCE.getCookie('cb_affpn'))) {
				clearCBUserExpCookie();
			}
		}
		
	} // end enableCookies conditional statement
}

runCBCookies();

$(window).load(function() {
	if (USEREXPERIENCE.getCookie('cb_affpn')) {
		$('.phone:not(.ignore)').html(USEREXPERIENCE.getCookie('cb_affpn'));
	}
});
